let curses = module.exports = {};

const pgp = require('pg-promise')();

const database = require('./index');

curses.addCurse = function( curseObject ) {
	let currentDateTime = new Date();

	curseObject["createdAt"] = currentDateTime;
	curseObject["updatedAt"] = currentDateTime;
	const query = pgp.helpers.insert( curseObject, ['doorID_FK', 'effectID_FK', 'createdAt', 'updatedAt'], 'Curses' );

	return database.none( query );
};

curses.readCurseCard = function( cardPK ) {
	return database.one('select * from "Curses" inner join "Player_Effects" on "Curses"."effectID_FK" = "Player_Effects"."playerEffectsID" inner join "Doors" on "Doors"."doorsID" = "Curses"."doorID_FK" inner join "Cards" on "Cards"."cardsID" = "Doors"."cardID_FK" where "cardsID" = $1', [cardPK]);
};

curses.deleteCurse = function( curseID ){
	return database.none( 'DELETE FROM "Curses" WHERE "cursesID" = $1', [curseID] );
};