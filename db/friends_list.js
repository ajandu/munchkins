let friendsList = module.exports = {};

const pgp = require('pg-promise')();

const database = require('./index');

friendsList.addFriendsList = function( friendsListObject ) {
	let currentDateTime = new Date();

	friendsListObject["createdAt"] = currentDateTime;
	friendsListObject["updatedAt"] = currentDateTime;
	const query = pgp.helpers.insert( friendsListObject, ['user_ID', 'friend_ID', 'createdAt', 'updatedAt'], 'Friends_Lists' );

	return database.none( query );
};

friendsList.readCard = function( friendsListID ) {
	return database.any( 'SELECT * FROM "Friends_Lists" WHERE id = $1', [friendsListID]);
};

friendsList.deleteCard = function( friendsListID ) {
	return database.none( 'DELETE FROM "Friends_Lists" WHERE id = $1', [friendsListID]);
};