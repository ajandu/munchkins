let cards = module.exports = {};

const pgp = require('pg-promise')();

const database = require('./index');

cards.addCard = function( cardObject ) {
	let currentDateTime = new Date();
	cardObject["createdAt"] = currentDateTime;
	cardObject["updatedAt"] = currentDateTime;

	return database.one('INSERT INTO "Cards"(name,type,"createdAt","updatedAt") VALUES(${name},${type},${createdAt},${updatedAt}) RETURNING "cardsID"',
	   {
	   name : cardObject.name,
	   type : cardObject.type,
	   createdAt : cardObject.createdAt,
	   updatedAt : cardObject.updatedAt});
};

cards.readCard = function( cardsID ) {
	return database.one('select * from "Cards" where "cardsID" = $1', [cardsID]);
};

cards.updateType = function( type, cardsID ){
	return database.none('UPDATE "Cards" SET type = $1 WHERE "cardsID" = $2', [type, cardsID]);
};

cards.deleteCard = function( cardsID ) {
	return database.none('DELETE FROM "Cards" WHERE "cardsID" = $1', [cardsID]);
};

cards.getAllCardAttributes = function ( cardsID ) {
	let x = 5;
	// userID = userID of card holder
	// returnedCardObj.userID = userID;  
	return database.one('SELECT "type" FROM "Cards" where "cardsID" = $1', [cardsID])
	.then((result) => {
		if( result.type === 0 ) {
			return database.one('SELECT * FROM "Treasures" INNER JOIN "Player_Effects" ON "Player_Effects"."playerEffectsID" = "Treasures"."effectID_FK" INNER JOIN "Cards" ON "Cards"."cardsID" = "Treasures"."cardID_FK" WHERE "cardsID" = $1', [cardsID]);
		} else if( result.type === 1 ) {
			return database.one('SELECT * FROM "Classes" INNER JOIN "Player_Effects" ON "Classes"."effectID_FK" = "Player_Effects"."playerEffectsID" INNER JOIN "Doors" ON "Classes"."doorID_FK" = "Doors"."doorsID" INNER JOIN "Cards" ON "Cards"."cardsID" = "Doors"."cardID_FK" WHERE "cardsID" = $1', [cardsID]);
		} else if( result.type === 3 ) {
			return database.one( 'SELECT * FROM "Monsters" INNER JOIN "Doors" ON "Monsters"."doorID_FK" = "Doors"."doorsID" INNER JOIN "Cards" ON "Cards"."cardsID" = "Doors"."cardID_FK" WHERE "cardsID" = $1', [cardsID] );
		} else {
			return database.one('SELECT * FROM "Curses" INNER JOIN "Player_Effects" ON "Curses"."effectID_FK" = "Player_Effects"."playerEffectsID" INNER JOIN "Doors" ON "Doors"."doorsID" = "Curses"."doorID_FK" INNER JOIN "Cards" on "Cards"."cardsID" = "Doors"."cardID_FK" WHERE "cardsID" = $1', [cardsID]);
		}
	})
	.catch((error) => {
		console.log(error);
	})
	
};
