let treasures = module.exports = {};

const pgp = require('pg-promise')();

const database = require('./index');

treasures.addTreasure = function( treasureObject ) {
	let currentDateTime = new Date();
	treasureObject["createdAt"] = currentDateTime;
	treasureObject["updatedAt"] = currentDateTime;

	return database.one('INSERT INTO "Treasures"(name,"goldAmount","cardID_FK","effectID_FK","createdAt","updatedAt") VALUES(${name},${goldAmount},${cardID_FK},${effectID_FK},${createdAt},${updatedAt}) RETURNING "treasuresID"',
	   {
	   name : treasureObject.name,
	   goldAmount : treasureObject.goldAmount,
	   cardID_FK: treasureObject.cardID_FK,
	   effectID_FK: treasureObject.effectID_FK,
	   createdAt : treasureObject.createdAt,
	   updatedAt : treasureObject.updatedAt});
};

treasures.returnCardForeignKeys = function ( ) {
	return database.many( 'SELECT "cardID_FK" FROM "Treasures"');
};

treasures.readTreasure = function( treasureID ){
	return database.any( 'SELECT * FROM "Treasures" WHERE id = $1', [treasureID] );
};

treasures.updateGoldAmount = function( updatedGoldAmount, treasureID ) {
	return database.none( 'UPDATE "Treasures" SET goldAmount = $1 WHERE id = $2', [updatedGoldAmount, treasureID] );
};

treasures.deleteTreasure = function( treasureID ){
	return database.none( 'DELETE FROM "Treasures" WHERE id = $1', [treasureID] );
};