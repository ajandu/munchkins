let doors = module.exports = {};

const pgp = require('pg-promise')();

const database = require('./index');

doors.addDoor = function( doorObject ) {
	let currentDateTime = new Date();
	doorObject["createdAt"] = currentDateTime;
	doorObject["updatedAt"] = currentDateTime;

	return database.one('INSERT INTO "Doors"(name,"cardID_FK","createdAt","updatedAt") VALUES(${name},${cardID_FK},${createdAt},${updatedAt}) RETURNING "doorsID"',
	   {
	   name : doorObject.name,
	   cardID_FK : doorObject.cardID_FK,
	   createdAt : doorObject.createdAt,
	   updatedAt : doorObject.updatedAt});
};


doors.returnCardForeignKeys = function ( ) {
	return database.many( 'SELECT "cardID_FK" FROM "Doors"');
};

doors.readDoor = function( doorID ){
	return database.any( 'SELECT * FROM "Doors" WHERE id = $1', [doorID] );
};

doors.updateCardForeignKey = function( updatedForeignKey, doorID ){
	return database.none( 'UPDATE "Doors" SET cardID_FK = $1 WHERE id = $2', [updatedForeignKey, doorID] );
};

doors.deleteDoor = function( doorID ){
	return database.none( 'DELETE FROM "Doors" WHERE id = $1', [doorID] );
};
