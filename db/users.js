let users = module.exports = {};
const pgp = require('pg-promise')();
const database = require('./index');


users.addUser = function( userObject ) {

	let currentDateTime = new Date();

	userObject["createdAt"] = currentDateTime;
	userObject["updatedAt"] = currentDateTime;

	userObject.games_played = 0;
	userObject.games_won = 0;

	const query = pgp.helpers.insert(userObject, ['email', 'username', 'password', 'games_played', 'games_won', 'createdAt', 'updatedAt'], 'Users');

	return database.none(query);
};

users.addUserReturnID = function( userObject ) {
	let currentDateTime = new Date(); 
	userObject["createdAt"] = currentDateTime;
	userObject["updatedAt"] = currentDateTime;

	return database.one('INSERT INTO "Users" (email,username,password,"games_played","games_won", "createdAt","updatedAt") VALUES(${email},${username},${password},${games_played},${games_won},${createdAt},${updatedAt}) RETURNING "usersID"',
	   {
	   email : userObject.email,
	   username : userObject.username,
	   password : userObject.password,
	   games_played : userObject.games_played,
	   games_won : userObject.games_won,
	   createdAt : userObject.createdAt,
	   updatedAt : userObject.updatedAt});
}


users.readUser = function( userID ) {

	return database.one('select * from "Users" where "usersID" = $1', [userID]);
};

users.updatePassword = function ( password, userID ) {

	return database.none('UPDATE "Users" SET password = $1 WHERE "usersID" = $2', [password, userID]);
};

users.updateGamesPlayed = function( gamesPlayed,userID ) {

	return database.none('UPDATE "Users" SET games_played = $1 WHERE "usersID" = $2', [gamesPlayed, userID]);
};

users.updateGamesWon = function( gamesWon, userID ) {

	return database.none('UPDATE "Users" SET games_won = $1 WHERE "usersID" = $2', [gamesWon, userID]);
};

users.deleteUser = function( userID ) {
	return database.one('DELETE FROM "Users" WHERE "usersID" = $1', [userID]);
};
