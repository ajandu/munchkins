let query = module.exports = {};

const pgp = require('pg-promise')();
const database = require('./index');

query.findUserByID = function (userID) {
	return database.one('select * from "Users" where "usersID" = $1', [userID]);
};

query.findUserByUsernamePassword = function (username, hashedPassword) {
	return database.one('select "usersID" from "Users" where username = $1 AND password = $2', [username, hashedPassword]);
};

query.findUserByUsername = function (username) {
	return database.one('select * from "Users" where username = $1', [username]);
};

query.getAllUsers = function() {
	return database.any('select * from "Users"');
};