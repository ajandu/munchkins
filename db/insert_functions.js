let insert = module.exports = {}; 

const pgp = require('pg-promise')();
const database = require('./index');

insert.addUser = function (userObject) {

	let currentDateTime = new Date(); 

	userObject["createdAt"] = currentDateTime; 
	userObject["updatedAt"] = currentDateTime; 

	userObject.games_played = 0; 
	userObject.games_won = 0; 

	const query = pgp.helpers.insert(userObject, ['email', 'username', 'password', 'games_played', 'games_won', 'createdAt', 'updatedAt'], 'Users');
	

	return database.none(query);

};