let gameEffects = module.exports = {};

const pgp = require('pg-promise')();

const database = require('./index');

gameEffects.addPlayerEffect = function( gameEffectsObject ) {
	let currentDateTime = new Date();
	gameEffectsObject["createdAt"] = currentDateTime;
	gameEffectsObject["updatedAt"] = currentDateTime;

	return database.one('INSERT INTO "Game_Effects"(description,effect,"createdAt","updatedAt") VALUES(${description},${effect},${createdAt},${updatedAt}) RETURNING gameEffectsID',
	   {
	   description : gameEffectsObject.description,
	   effect : gameEffectsObject.effect,
	   createdAt : gameEffectsObject.createdAt,
	   updatedAt : gameEffectsObject.updatedAt});
};

gameEffects.readGameEffect = function( gameEffectID ){
	return database.any( 'SELECT * FROM "Game_Effects" WHERE id = $1', [gameEffectID] );
};

gameEffects.updateGameEffect = function( updatedEffect, gameEffectID ){
	return database.none( 'UPDATE "Game_Effects" SET effect = $1 WHERE id = $2', [updatedEffect, gameEffectID] );
};

gameEffects.deleteGameEffect = function( gameEffectID ){
	return database.none( 'DELETE FROM "Game_Effects" WHERE id = gameEffectID' );
};