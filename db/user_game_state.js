let userGameState = module.exports = {};

const pgpX = require('pg-promise')();

const database = require('./index');

userGameState.addUserGameState = function (userGameStateObject) {

	return database.none('INSERT INTO "User_Game_States" ("userID_FK", "gameID_FK", "userHand", "cardsInPlay", "currentUserPhase", "userClass", "classValue", "userEquipment", "equipmentValue", "userLevel", "userStrength") VALUES (${userID_FK}, $(gameID_FK) , ${userHand}, ${cardsInPlay}, ${currentUserPhase}, ${userClass}, ${classValue}, ${userEquipment}, ${equipmentValue}, ${userLevel}, ${userStrength})', {
		userID_FK: userGameStateObject.userID_FK,
		gameID_FK: userGameStateObject.gameID_FK,
		userHand: userGameStateObject.userHand,
		cardsInPlay: userGameStateObject.cardsInPlay,
		currentUserPhase: userGameStateObject.currentUserPhase,
		userClass: userGameStateObject.userClass,
		classValue: userGameStateObject.classValue,
		userEquipment: userGameStateObject.userEquipment,
		equipmentValue: userGameStateObject.equipmentValue,
		userLevel: userGameStateObject.userLevel,
		userStrength: userGameStateObject.userStrength
	});
};

userGameState.updateGameUserState = function (userGameStateObject) {
	return database.one('UPDATE "User_Game_States" SET ("userHand", "cardsInPlay", "currentUserPhase", "userClass", "classValue", "userEquipment", "equipmentValue", "userLevel", "userStrength") VALUES (${userID_FK}, ${userHand}, ${cardsInPlay}, ${currentUserPhase}, ${userClass}, ${classValue}, ${userEquipment}, ${equipmentValue}, ${userLevel}, ${userStrength}) WHERE "userID_FK" = ${userID_FK}', {
		userID_FK: userGameStateObject.userID_FK,
		userHand: userGameStateObject.userHand,
		cardsInPlay: userGameStateObject.cardsInPlay,
		currentUserPhase: userGameStateObject.currentUserPhase,
		userClass: userGameStateObject.userClass,
		classValue: userGameStateObject.classValue,
		userEquipment: userGameStateObject.userEquipment,
		equipmentValue: userGameStateObject.equipmentValue,
		userLevel: userGameStateObject.userLevel,
		userStrength: userGameStateObject.userStrength
	})
	promise.then((value) => {

		})
		.promise.catch(error => {
			console.log(error)
		})
};

userGameState.getAllUsersInGame = function (gameRoom) {
	return database.many('select * from "Users" INNER JOIN "User_Game_States"  on "Users"."usersID" =  "User_Game_States"."userID_FK"  WHERE "gameID_FK" = $1', [gameRoom])

};


userGameState.addUserGameStateReturnResult = function (userGameStateObject) {
	return database.one('INSERT INTO "User_Game_States" ("userID_FK", "userHand", "cardsInPlay", "currentUserPhase", "userClass", "classValue", "userEquipment", "equipmentValue", "userLevel", "userStrength") VALUES (${userID_FK}, ${userHand}, ${cardsInPlay}, ${currentUserPhase}, ${userClass}, ${classValue}, ${userEquipment}, ${equipmentValue}, ${userLevel}, ${userStrength})', {
			userID_FK: userGameStateObject.userID_FK,
			userHand: userGameStateObject.userHand,
			cardsInPlay: userGameStateObject.cardsInPlay,
			currentUserPhase: userGameStateObject.currentUserPhase,
			userClass: userGameStateObject.userClass,
			classValue: userGameStateObject.classValue,
			userEquipment: userGameStateObject.userEquipment,
			equipmentValue: userGameStateObject.equipmentValue,
			userLevel: userGameStateObject.userLevel,
			userStrength: userGameStateObject.userStrength
		})
		.then((value) => {

		})
	promise.catch(error => {
		console.log(error);
	})
};




userGameState.addCardToHand = function (cardPK, usersID, gameID) {
	return database.none('UPDATE "User_Game_States" SET "userHand" = array_append ("userHand", $1) WHERE "userID_FK" = $2 AND "gameID_FK" = $3 ', [cardPK, usersID, gameID])
		.then((result) => {
			console.log("updated hand!");
		})
		.catch((error) => {
			console.log(error);
		});
};

userGameState.removeCardFromHand = function (cardPK, usersID, gameID) {
	return database.none('UPDATE "User_Game_States" SET "userHand" = array_remove ("userHand", $1) WHERE "userID_FK" = $2 AND "gameID_FK" = $3 ', [cardPK, usersID, gameID])
		.then((result) => {})
		.catch((error) => {
			console.log(error);
		});
};

userGameState.getHand = function (usersID, gameID) {
	return database.one('SELECT "userHand" FROM "User_Game_States" WHERE "userID_FK" = $1 AND "gameID_FK" = $2 ', [usersID, gameID])
};

userGameState.emptyHand = function (newHand, usersID, gameID) {
	return database.none('UPDATE "User_Game_States" SET "userHand" = $1  WHERE "userID_FK" = $2 AND "gameID_FK" = $3 ', [newHand, usersID, gameID])
};

userGameState.updateUserLevel = function (updatedLevel, usersID, gameID) {
	return database.none('UPDATE "User_Game_States" SET "userLevel" = $1 WHERE "userID_FK" = $2 AND "gameID_FK" = $3', [updatedLevel, usersID, gameID])
		.then((result) => {
			console.log("level updated!");
		})
		.catch((error) => {
			console.log(error);
		});
};

userGameState.getUserLevel = function (usersID, gameID) {
	return database.one('SELECT "userLevel" FROM "User_Game_States" WHERE "userID_FK" = $1 AND "gameID_FK" = $2 ', [usersID, gameID])
};

userGameState.updateCurrentPhase = function (updatedPhase, usersID, gameID) {
	return database.none('UPDATE "User_Game_States" SET "currentUserPhase" = $1 WHERE "userID_FK" = $2 AND "gameID_FK" = $3', [updatedPhase, usersID, gameID]);
};

userGameState.getCurrentPhase = function (usersID, gameID) {
	return database.one('SELECT "currentUserPhase" FROM "User_Game_States" WHERE "userID_FK" = $1 AND "gameID_FK" = $2', [usersID, gameID]);
};

userGameState.updateUserStrength = function (usersID, gameID) {
	var userStrength = 0;
	return database.one('select * from "User_Game_States" WHERE "userID_FK" = $1 AND "gameID_FK" = $2', [usersID, gameID])
		.then((result) => {
			console.log(result, {
				depth: null
			})
			userStrength += result.classValue;
			userStrength += result.equipmentValue;
			userStrength += result.userLevel;
			console.log(userStrength);
			return database.none('UPDATE "User_Game_States" SET "userStrength" = $1 WHERE "userID_FK" = $2 AND "gameID_FK" = $3', [userStrength, result.userID_FK, result.gameID_FK])
		})
		.catch((error) => {
			console.log(error);
		});
};

userGameState.getUserStrength = function (usersID, gameID) {
	return database.one('SELECT "userStrength" FROM "User_Game_States" WHERE "userID_FK" = $1 AND "gameID_FK" = $2', [usersID, gameID]);
};

userGameState.setClassValue = function (classValue, usersID, gameID) {
	return database.none('UPDATE "User_Game_States" SET "classValue" = $1 WHERE "userID_FK" = $2 AND "gameID_FK" = $3', [classValue, usersID, gameID])
		.catch((error) => {
			console.log(error);
		});
};


userGameState.updateClassValue = function (cardPK, usersID, gameID) {
	return database.one('select * from "Classes" INNER JOIN "Player_Effects" ON "Classes"."effectID_FK" = "Player_Effects"."playerEffectsID" INNER JOIN "Doors" ON "Doors"."doorsID" = "Classes"."doorID_FK" INNER JOIN "Cards" ON "Cards"."cardsID" = "Doors"."cardID_FK" WHERE "cardsID" = $1', [cardPK])
		.then((classResult) => {
			return database.none('UPDATE "User_Game_States" SET "classValue" = $1 WHERE "userID_FK" = $2 AND "gameID_FK" = $3', [classResult.effect, usersID, gameID])
				.then((result) => {
					console.log("Updated Class Value Successfully!");
				})
				.catch((error) => {
					console.log(error);
				});
		})
		.catch((error) => {
			console.log(error);
		});

};


userGameState.updateEquipmentValue = function (usersID, gameID) {
	return database.one('SELECT * FROM "User_Game_States" WHERE "userID_FK" = $1 AND "gameID_FK" = $2', [usersID, gameID])
		.then((resultGameState) => {
			console.log('inside UpdateEquipmentValue')
			console.log(resultGameState, {
				depth: null
			})
			var equipmentValue = 0;
			if (resultGameState.userEquipment !== null) {
				return Promise.all(resultGameState.userEquipment.map((equipmentCards) => {
						return database.one('select * from "Treasures" INNER JOIN "Cards" ON "Treasures"."cardID_FK" = "Cards"."cardsID" INNER JOIN "Player_Effects" ON "Treasures"."effectID_FK" = "Player_Effects"."playerEffectsID" WHERE "cardsID" = $1', [equipmentCards])
							.then(result => equipmentValue += result.effect)
					}))
					.then(() => {
						return database.none('UPDATE "User_Game_States" SET "equipmentValue" = $1 WHERE "userID_FK" = $2 AND "gameID_FK" = $3', [equipmentValue, resultGameState.userID_FK, resultGameState.gameID_FK])
					})
			} else {
				console.log("No equipment to process!");
			}
		})
		.catch((error) => {
			console.log(error);
		})
};

userGameState.addEquipmentCard = function (cardPK, usersID, gameID) {
	return database.none('UPDATE "User_Game_States" SET "userEquipment" = array_append ("userEquipment", $1) WHERE "userID_FK" = $2 AND "gameID_FK" = $3', [cardPK, usersID, gameID])
		.then((result) => {
			console.log("updated equipment!");
		})
		.catch((error) => {
			console.log(error);
		});
};

userGameState.getEquipmentCards = function (usersID, gameID) {
	return database.one('SELECT * FROM "User_Game_States" WHERE "userID_FK" = $1 AND "gameID_FK" = $2', [usersID, gameID]);
};

userGameState.removeEquipmentCard = function (cardPK, usersID, gameID) {
	return database.none('UPDATE "User_Game_States" SET "userEqupiment" = array_remove ("userEquipment", $1) WHERE "userID_FK" = $1 AND "gameID_FK" = $2', [usersID, gameID])
		.catch((error) => {
			console.log(error);
		});
};

userGameState.updateEquipmentCards = function (equipmentArray, usersID, gameID) {
	console.log("Equipment Array Will Now Be: " + equipmentArray);
	return database.one('UPDATE "User_Game_States" SET "userEquipment" = $1 WHERE "userID_FK" = $2 and "gameID_FK" = $3 RETURNING "equipmentValue"', [equipmentArray, usersID, gameID]);

};