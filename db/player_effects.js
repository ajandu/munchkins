let playerEffects = module.exports = {};

const pgp = require('pg-promise')();

const database = require('./index');

playerEffects.addPlayerEffect = function( playerEffectsObject ) {
	let currentDateTime = new Date();
	playerEffectsObject["createdAt"] = currentDateTime;
	playerEffectsObject["updatedAt"] = currentDateTime;
	return database.one('INSERT INTO "Player_Effects"(description,effect,"createdAt","updatedAt") VALUES(${description},${effect},${createdAt},${updatedAt}) RETURNING "playerEffectsID"',
	   {
	   description : playerEffectsObject.description,
	   effect : playerEffectsObject.effect,
	   createdAt : playerEffectsObject.createdAt,
	   updatedAt : playerEffectsObject.updatedAt});
};

playerEffects.readPlayerEffect = function( playerEffectID ) {
	return database.any( 'SELECT * FROM "Player_Effects" WHERE id = $1', [playerEffectID]);
};

playerEffects.updateEffect = function( updatedEffect ){
	return database.none( 'UPDATE "Player_Effects" SET effect = $1', [updatedEffect]);
};

playerEffects.deleteEffect = function( playerEffectID ){
	return database.none( 'DELETE FROM "Player_Effects" WHERE id = $1', [playerEffectID]);
};