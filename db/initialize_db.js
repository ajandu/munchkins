let initialize_db = module.exports = {};

const populateDatabase = require('./populate_functions');
const fs = require("fs");

initialize_db.databaseInit = function() {
	const playerCurse = fs.readFileSync("./assets/curses.json");
	let playerCurseContent = JSON.parse(playerCurse);
	let numberOfCurses = 4;
	for( let index = 0; index < numberOfCurses; index++ ) {
	  populateDatabase.populateCurses( playerCurseContent.curses[index].name, 4, playerCurseContent.curses[index].effect,
		playerCurseContent.curses[index].numberOfCards, playerCurseContent.curses[index].description );
	}

	const monsterJSON = fs.readFileSync( "./assets/monsters.json" );
	const monsterContent = JSON.parse( monsterJSON );
	let numberOfMonsters = 12;

	for( let index = 0; index < numberOfMonsters; index++ ) {
	  populateDatabase.populateMonsters( monsterContent.monsters[index].name, 3, monsterContent.monsters[index].strength, monsterContent.monsters[index].rewards,
		monsterContent.monsters[index].level_up_amount, monsterContent.monsters[index].numberOfCards );
	}

	const classesJSON = fs.readFileSync( "./assets/classes.json" );
	const classesContent = JSON.parse( classesJSON );
	let numberOfClasses = 3;

	for( let index = 0; index < numberOfClasses; index++ ) {
	  populateDatabase.populateClasses( classesContent.classes[index].name, 1, classesContent.classes[index].effect,
		classesContent.classes[index].numberOfCards, classesContent.classes[index].description );
	}

	const equipmentJSON = fs.readFileSync( "./assets/equipment.json" );
	const equipmentContent = JSON.parse( equipmentJSON );
	let numberOfEquipment = 8;

	for( let index = 0; index < numberOfEquipment; index++ ) {
	  populateDatabase.populateEquipment( equipmentContent.equipment[index].name, 0, equipmentContent.equipment[index].effect,
		equipmentContent.equipment[index].goldAmount, equipmentContent.equipment[index].numberOfCards, equipmentContent.equipment[index].description );
	}
};