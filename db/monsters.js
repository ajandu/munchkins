let monsters = module.exports = {};

const pgp = require('pg-promise')();

const database = require('./index');

monsters.addMonster = function( monsterObject ) {
	let currentDateTime = new Date();

	monsterObject["createdAt"] = currentDateTime;
	monsterObject["updatedAt"] = currentDateTime;
	const query = pgp.helpers.insert( monsterObject, ['doorID_FK', 'strength', 'rewards', 'level_up_amount', 'createdAt', 'updatedAt'], 'Monsters' );

	return database.none( query );
};

monsters.readMonster = function( cardPK ) {
	return database.one( 'SELECT * FROM "Monsters" INNER JOIN "Doors" ON "Monsters"."doorID_FK" = "Doors"."doorsID" INNER JOIN "Cards" ON "Cards"."cardsID" = "Doors"."cardID_FK" WHERE "cardsID" = $1', [cardPK] );
};

monsters.updateStrength = function( strength, monsterID ) {
	return database.none( 'UPDATE "Monsters" SET strength = $1 WHERE id = $2', [strength, monsterID] );
};

monsters.updateRewards = function( rewards, monsterID ) {
	return database.none( 'UPDATE "Monsters" SET rewards = $1 WHERE id = $2', [rewards, monsterID] );
};

monsters.updateLevelUpAmount = function( levelUpAmount, monsterID ) {
	return database.none( 'UPDATE "Monsters" SET level_up_amount = $1 WHERE id = $2', [levelUpAmount, monsterID] );
};

monsters.deleteMonster = function( monsterID ) {

	return database.one('DELETE FROM "Monsters" WHERE id = $1', [monsterID]);
};