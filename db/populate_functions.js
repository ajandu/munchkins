let db = module.exports = {};

const pgp = require('pg-promise')();
const database = require('./index');
const card = require('./cards');
const door = require('./doors');
const treasure = require('./treasures');
const player_effect = require('./player_effects');
const curse = require('./curses');
const classes = require('./class');
const monster = require('./monsters');

db.populateCurses = function( name, type, effect, numberOfCards, description ) {
  for( let index = 0; index < numberOfCards; index++ ) {
    const cardObject = {
      name: name,
      type: type
    };

    card.addCard( cardObject )
    .then(( resultCard ) => {
      const doorObject = {
        name: name,
        cardID_FK: resultCard.cardsID
      };
      door.addDoor( doorObject )
      .then((resultDoor) => {
        const playerEffectObject = {
          description: description,
          effect: effect
        };
        player_effect.addPlayerEffect( playerEffectObject )
        .then((resultPlayerEffect) => {
          const curseObject = {
            doorID_FK: resultDoor.doorsID,
            effectID_FK: resultPlayerEffect.playerEffectsID
          };
          curse.addCurse(curseObject)
          .then((resultCurse) => {
          })
          .catch((error)=>{
            console.log(error);
          });
        })
        .catch((error) => {
          console.log(error);
        });
      })
      .catch((error) => {
        console.log(error);
      });
    })
    .catch(( error ) => {
      console.log(error)
    });
  }
};

db.populateMonsters = function( name, type, strength, rewards, level_up_amount, numberOfCards ) {
  for( let index = 0; index < numberOfCards; index++ ){
    const cardObject = {
      name: name,
      type: type
    };

    card.addCard( cardObject )
    .then(( resultCard ) => {
      const doorObject = {
        name: name,
        cardID_FK: resultCard.cardsID
      };
      door.addDoor( doorObject )
      .then(( resultDoor ) => {
        const monsterObject = {
          doorID_FK: resultDoor.doorsID,
          strength: strength,
          rewards: rewards,
          level_up_amount: level_up_amount
        };
        monster.addMonster( monsterObject )
        .then(( result ) => {
        })
        .catch(( error ) => {
        });
      })
      .catch(( error ) => {
        console.log( error );
      });
    })
    .catch(( error ) => {
      console.log(error);
    })
  }
};

db.populateClasses = function( name, type, effect, numberOfCards, description ) {
  for( let index = 0; index < numberOfCards; index++ ) {
    const cardObject = {
      name: name,
      type: type
    };
    card.addCard( cardObject )
    .then(( resultCard ) => {
      const doorObject = {
        name: name,
        cardID_FK: resultCard.cardsID
      };
      door.addDoor( doorObject )
      .then(( resultDoor ) => {

        const playerEffectObject = {
          description: description,
          effect: effect
        };

        player_effect.addPlayerEffect( playerEffectObject )
        .then(( resultPlayerEffect ) => {
          const classObject = {
            name: name,
            effectID_FK: resultPlayerEffect.playerEffectsID,
            doorID_FK: resultDoor.doorsID
          };
          classes.addClass( classObject )
          .then((result) => {
          })
          .catch(( error ) => {
            console.log(error);
          });
        })
        .catch(( error ) => {
          console.log( error );
        });
      })
      .catch(( error ) => {
        console.log( error );
      });
    })
    .catch(( error ) => {
      console.log(error);
    });
  }
};

db.populateEquipment = function( name, type, effect, goldAmount, numberOfCards, description ) {
   for( let index = 0; index < numberOfCards; index++ ) {
    const cardObject = {
      name: name,
      type: type
    };
    card.addCard( cardObject )
    .then(( resultCard ) => {
      const playerEffectObject = {
          description: description,
          effect: effect
      };
      player_effect.addPlayerEffect( playerEffectObject )
      .then((resultPlayerEffect) => {
        const treasureObject = {
          name: name,
          goldAmount: goldAmount,
          cardID_FK: resultCard.cardsID,
          effectID_FK: resultPlayerEffect.playerEffectsID
        };
        treasure.addTreasure( treasureObject )
        .then( ( resultTreasure ) => {
        })
        .catch(( error ) => {
          console.log("FUCKED UP ADDING TREASURE *********************");
          console.log(error);
        });
      })
      .catch(( error ) => {
        console.log("FUCKED UP ADDING PLAYER EFFECT! *************");
        console.log(error);
      });
    })
    .catch(( error ) => {
      console.log("FUCKED UP ADDING CARDS ********************");
      console.log(error);
    });
  }
};