let json_test = module.exports = {};

const pgp = require('pg-promise')();

const database = require('./index');

json_test.addTestObject = function( testObject ) {
	const query = pgp.helpers.insert(testObject, ['firstname','lastname'], 'jsonexample');

	return database.none( query );
};

json_test.readCard = function( cardID ) {
	return database.any('select * from "jsonexample" where id = $1', [userID]);
};

json_test.updateType = function( type, cardID ){
	return database.none('UPDATE "jsonexample" SET type = $1 WHERE id = $2', [type, cardID]);
};

json_test.deleteCard = function( cardID ) {
	return database.none('DELETE FROM "jsonexample" WHERE id = $1', [cardID]);
};
