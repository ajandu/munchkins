let messages = module.exports = {};

const pgp = require('pg-promise')();

const database = require('./index');

messages.addMessages = function( messageObject ) {
	let currentDateTime = new Date();

	messageObject["createdAt"] = currentDateTime;
	messageObject["updatedAt"] = currentDateTime;
	const query = pgp.helpers.insert( messageObject, ['user_id', 'message_body', 'createdAt', 'updatedAt'], 'Messages' );

	return database.none( query );
};

messages.readMessage = function( messageID ){
	return database.any( 'SELECT * FROM "Messages" WHERE id = $1', [messageID] );
};

messages.updateMessageBody = function( updatedBody, messageID ){
	return database.none( 'UPDATE FROM "Messages" SET message_body = $1 WHERE id = $2', [updatedBody, messageID]);
};

messages.deleteMessage = function( messageID ){
	return database.none( 'DELETE FROM "Messages" WHERE id = $1', [messageID] );
};