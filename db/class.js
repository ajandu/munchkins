 let classCard = module.exports = {};

const pgp = require('pg-promise')();

const database = require('./index');

classCard.addClass = function( classCardObject ) {
	let currentDateTime = new Date();

	classCardObject["createdAt"] = currentDateTime;
	classCardObject["updatedAt"] = currentDateTime;

	let query = pgp.helpers.insert( classCardObject, ['name', 'effectID_FK', 'doorID_FK', 'createdAt', 'updatedAt'], 'Classes');
	return database.none( query );
};

classCard.readClass = function( classCardID ) {
	return database.any('SELECT * FROM "Classes" WHERE id = $1', [classCardID] );
};

classCard.deleteClass = function( classCardID ){
	return database.none('DELETE FROM "Classes" WHERE id = $1', [classCardID]);
};