let games = module.exports = {};

const pgp = require('pg-promise')();

const database = require('./index');

//Create
games.addGame = function( gameObject ) { 
	/* addGame returns a promise now that inserts a game into the Games table. When resolved,
		 it returns the newly created gameID */
	return new Promise (
		(resolve, reject) => {
		let currentDateTime = new Date();
			
		var createdGameID;
		database.one('INSERT INTO "Games"("userList","treasureDeck","treasureDiscard","doorDeck","doorDiscard","cardsInPlay","currentUserTurn") VALUES ($1, $2, $3, $4, $5, $6, $7) RETURNING "gamesID"', [gameObject["userList"], gameObject["treasureDeck"], gameObject["treasureDiscard"],gameObject["doorDeck"],gameObject["doorDiscard"],gameObject["cardsInPlay"],gameObject["currentUserTurn"]])
			.then(data => {
				resolve(data.gamesID);
			})
			.catch(error =>{
				console.log('ERROR:', error);
			})
	});
};

games.getGameState = function(gamesID){
	return database.one('SELECT * FROM "Games" WHERE "gamesID" = $1', [gamesID])
}

games.readDoorDeck = function( gamesID ) {
	return database.one('SELECT "doorDeck" FROM "Games" WHERE "gamesID" = $1', [gamesID]);
};

games.readTreasureDeck = function( gamesID ) {
	return database.one('SELECT "treasureDeck" FROM "Games" WHERE "gamesID" = $1', [gamesID]);
};

games.getUsers = function ( gamesID ) {
	return database.one('SELECT "userList" FROM "Games" WHERE "gamesID" = $1', [gamesID]);
};

games.readCardsInPlay = function( gamesID ) {
	return database.one('SELECT "cardsInPlay" FROM "Games" WHERE "gamesID" = $1', [gamesID]);	
};

games.addCardInPlay = function( cardInPlay, gamesID ) {
	// database.none( 'UPDATE "Games" SET "cardsInPlay" = array_append("cardsInPlay", $1) WHERE "gamesID" = $2 RETURNING "cardsInPlay" ', [cardInPlay, gamesID] )
	// .catch((error) => {
	// 	console.log(error);
	// });
	return database.one('WITH "updateCardsInPlay" AS (UPDATE "Games" SET "cardsInPlay" = array_append("cardsInPlay", $1) WHERE "gamesID" = $2 RETURNING *) SELECT "cardsInPlay" FROM "updateCardsInPlay" WHERE "gamesID" = $2', [cardInPlay, gamesID]);

};

// userGameState.updatePhaseTest = function() {
// 	return database.one('WITH "updatedPhase" AS (UPDATE "User_Game_States" SET "currentUserPhase" = 100 WHERE "userID_FK" = 1 RETURNING "currentUserPhase") SELECT "currentUserPhase" FROM "updatedPhase"');
// }

games.removeCardInPlay = function( cardInPlay, gamesID ) {
	return database.none( 'UPDATE "Games" SET "cardsInPlay" = array_remove("cardsInPlay", $1) WHERE "gamesID" = $2', [cardInPlay, gamesID] );
};

games.updateTreasureDeck = function( treasureDeck, gamesID ) {
	return database.none('UPDATE "Games" SET "treasureDeck" = $1 WHERE "gamesID" = $2', [treasureDeck,gamesID]);
};

games.updateDoorDeck = function( doorDeck, gamesID ) {
	return database.none('UPDATE "Games" SET "doorDeck" = $1 WHERE "gamesID" = $2', [doorDeck, gamesID]);
};

games.insertUser = function (userId, room){
	return database.query('UPDATE "Games" SET "userList" = array_append("userList", $1) WHERE "gamesID" = $2', [userId, room]);
};

//use this one when you're applying the skip turn curse
games.updateCurrentUserTurn = function( currentUserTurn, gamesID ) {
	return database.none('UPDATE "Games" SET "currentUserTurn" = $1 WHERE "gamesID" = $2', [currentUserTurn, gamesID]);
};

games.deleteUserFromUserList = function( usersID, gamesID ) {
	return database.none( 'UPDATE "Games" SET "userList" = array_remove("userList", $1) WHERE "gamesID" = $2', [usersID, gamesID] );
};

games.deleteGame = function( gamesID ) {
	return database.none( 'DELETE FROM "Games" WHERE "gamesID" = $1', [gamesID]);
};

games.dealCardFromDoorDeck = function( cardPK, gamesID ) {
	return database.none('UPDATE "Games" SET "doorDeck" = array_remove("doorDeck", $1) WHERE "gamesID" = $2', [cardPK, gamesID]);
};

games.dealCardFromTreasureDeck = function( cardPK, gamesID ) {
	return	database.none('UPDATE "Games" SET "treasureDeck" = array_remove("treasureDeck", $1) WHERE "gamesID" = $2', [cardPK, gamesID]);
};

