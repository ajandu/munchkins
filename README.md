# Munchkin

This is a web-server that lets you play a simplified versions of munchkins!

### Prerequisites

PostgreSQL : v.10.0
Node : 8.6.0
Google Chrome : Latest Version
Safari : Latest Version

### Installing

put the pre run scripts here for the database migrations to be run correctly.

npm run start:dev

Point your web browser to localhost:3000

## Game Design

## OverFlow

The appication uses a model view controller design to manage the various componenents of the game. This ensures that the codebase is maintainable, and modular.

### *INSERT PICTURE OF MVC LAYOUT *###


###Models

​	[/db/] (https://github.com/sfsu-csc-667-fall-2017/term-project-667-pr0-wizards/tree/master/db) Houses information for models within the database

​	[/migrations/] Provides the relationships for the database, files are named to follow sequence

### Views

​	[/views/](https://github.com/sfsu-csc-667-fall-2017/term-project-667-pr0-wizards/tree/master/views) Houses the templates to rendered to the screen

​	[/public/](https://github.com/sfsu-csc-667-fall-2017/term-project-667-pr0-wizards/tree/master/public) Houses all the client side (css/js) that is to be sent to the client

### Controller

​	[/routes/] Houses routing information for the requests that enter the server

​	[/socket/] Houses socket information to handle the information flow from application to the client 

​	[/engine_manager/] Houses information for how the socket functions should be handled, game logic stored here

## Frontend
 
### pug
    Pug as a templating engine introducted its own form of benefits and challenges. 

    Any minor error in the pug code would break the entire page. Be it from a missing tab that was putting a specific pug element too low or far too deep. Furthermore, when attempting to refactor code to include partials, pug would also break entirely for top level tags that seemed to work sometimes but not others. 

    Although pug is far nicer to look at, it makes you miss explicit delimitations and lax rules from html. 

### Jquery 

#### Assigning events to dynamically created elements
    To create click events tied to specific card objects that were not only dynamically assigned to the user object, but dynamically created on the DOM, proved to be an interesting challenge. 
    
    The hand limit was set to ten and needed to be displayed during the default state when looking at the board. So, we hard coded id's to each pug element to enable us to rely on that consistency. 
    
    Furthermore, since the cards in the hand were generated iteratively through the handList, the index 0 of handList relates to #0, index 1 of handList relates to #1, and so on, it enabled us to pass the id straight to the handList, retrieve the card object, then add that to an object named "cardsSelected", using the ID as a key. If the card is deselected, we check if the key (our handList index for the card object) already exists in the list. If it does, that key is deleted from the cardsSelected object. When the user selects an action, be it sell, discard, or equip, was selected, those cards are disgarded from the hand and sent to their appropriate location. 


## Problems Encountered (Front End - AJ)

### Gameboard Layout
    Most of the problems I encountered regarding the game board layout stemmed from not being familiar with grid and flexbox. I could have used Bootstrap's grid system, but I thought it would be a good opportunity to learn some new tools. The first few layouts I created were made using only flexbox because I thought that you either pick one or the other, but I found that using grid and flexbox in unison made creating layouts much easier. Grid was especially useful when creating the gameboard layout because the entire gameboard is mostly rendering cards, then flexbox was used within those cards to get the little items for each card in place we wanted.

### Dynamically Rendering the Hand
    I also ran into some problems when dynamically rendering the cards for each player. There are different types of cards in our game, and each type has its own layout with their own icons and information to be displayed on the card. I thought about appending "card" divs into our "cards-in-hand" div with css classes corresponding to each layout, but the way we tied our backend together with our frontend prevented this approach. My solution was to use a switch statement that checks what type of card is supposed to be rendered, and then it sets the html elements within the card to follow the corresponding layout, and hides parts of the layout if it's not needed.

    Rendering the rest of the board was not as big of a problem because our backend team worked hard to send the needed data to each client, so that data just had to be parsed and placed in the correct place.
    

### CSS 

#### Flexbox

#### CSS Grid

## Deployment

This code is fully deployable with Heroku!

## Built With

* [NodeJS](https://nodejs.org/en/) - Javascript Serverside Runtime
* [Express](http://expressjs.com) - Route Dispatching
* [Socket.io](https://socket.io) - Used to manage payload to the application
* [Sessions](https://www.npmjs.com/package/express-session) - Managing sessions for users on the server. 

## Versioning

Released Version 3.0

## Authors

* **Gerren Panolosa** - *Backend / Game Engine Design / Database Design* - [HeyImGerren](https://github.com/HeyImGerren) *
* **Amar Jandu** - *Backend / Game Engine Design / Application / Controller Design* - [amarjandu](https://github.com/amarjandu) *
* **Ariel Jonas Culanay** - *Frontend / Layout Design / Dynamic Rendering* - [aculanay](https://github.com/aculanay) *
* **Andrew Patterson** - *Frontend / Layout / Dynamic Rendering / Controller Design* - [andrewpat24](https://github.com/andrewpat24) *


## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

<<<<<<< HEAD
* zSoc
=======
* zSoc

## Problems Encountered (Front End - AJ)

### Gameboard Layout
    Most of the problems I encountered regarding the game board layout stemmed from not being familiar with grid and flexbox. I could have used Bootstrap's grid system, but I thought it would be a good opportunity to learn some new tools. The first few layouts I created were made using only flexbox because I thought that you either pick one or the other, but I found that using grid and flexbox in unison made creating layouts much easier. Grid was especially useful when creating the gameboard layout because the entire gameboard is mostly rendering cards, then flexbox was used within those cards to get the little items for each card in place we wanted.

### Dynamically Rendering the Hand
    I also ran into some problems when dynamically rendering the cards for each player. There are different types of cards in our game, and each type has its own layout with their own icons and information to be displayed on the card. I thought about appending "card" divs into our "cards-in-hand" div with css classes corresponding to each layout, but the way we tied our backend together with our frontend prevented this approach. My solution was to use a switch statement that checks what type of card is supposed to be rendered, and then it sets the html elements within the card to follow the corresponding layout, and hides parts of the layout if it's not needed. In order to clean this up, I wrote a jQuery function that's called on a jQuery selector that does all of the card rendering.

    Rendering the rest of the board was not as big of a problem because our backend team worked hard to send the needed data to each client, so that data just had to be parsed and placed in the correct place.
>>>>>>> aj
