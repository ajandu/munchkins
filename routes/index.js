const express = require('express');
const router = express.Router();
const insert = require('../db/insert_functions');
const query = require('../db/query_functions');
const passport = require('passport');

const bcrypt = require('bcrypt');
const saltRounds = 10;

const nodemailer = require('nodemailer');

const transporter = nodemailer.createTransport({
  service: 'gmail',
  host: 'smtp.gmail.com',
  auth: {
    user: process.env.EMAIL_ADDRESS,
    pass: process.env.EMAIL_PASSWORD
  }
});



router.get('/' ,function(request, response, next){
	response.render('account-forms/login');
});

router.get('/create-account', function(request, response, next){
  response.render('account-forms/create-account');
});

router.get('/forgot-password', function(request, response, next){
  response.render('account-forms/forgot-password');
});

router.get('/add-user', function(request, response, next) {
	response.redirect('/profile');
});

router.get('/profile', authenticationMiddleware('/'), function(request, response) {
		console.log('profile ID: '+request.session.passport.user);
		console.log('profile UserNAME: '+request.user.userName);  
	response.render('profile', {
		userName: request.user.username, 
		email: 'gerrenOrAmarAreJustOk@steveJobs.com',
		userID: parseInt(request.user.usersID)
	});
});

router.get('/login', function(request, response, next) {
	response.render('account-forms/login');
});

router.get('/logout', function(request, response, next) {
	request.logout; 
	request.session.destroy(); 
	response.redirect('/');
});

router.post('/login', passport.authenticate('local', {
	successRedirect: '/profile', 
	failureRedirect: '/login'
}));


router.post('/add-user', function(request, response, next) {
	console.log(request.body.password, request.body.passwordConfirmation);

	request.checkBody('userName', 'Username field connot be empty.').notEmpty();
	request.checkBody('userName', 'Username must be between 2 and 40 characters long').len(2, 40);

	request.checkBody('email', 'Email field must be a valid email address.').isEmail(); 
	request.checkBody('email', 'Email must be between 4 and 100 characters long.').len(4, 100); 

	request.checkBody('password', 'Password must be between 4 and 100 characters long').len(4, 100);

	request.checkBody('passwordConfirmation', 'Both passwords must be matching').isEqual(request.body.password);

	const errors = request.validationErrors();

	if(errors) {
		console.log('errors:' + JSON.stringify(errors));

		response.render('account-forms/create-account', {
			errors:errors
		});

	} else {

		if(request.body.password == request.body.passwordConfirmation && request.body.password.length >= 4) {

			bcrypt.hash(request.body.password, saltRounds, function(error, hash) {

				const userObj = {
					email: request.body.email, 
					password: hash, 
					username: request.body.userName
				};

				insert.addUser(userObj).then(function () {
					query.findUserByUsernamePassword(userObj.username, userObj.password)
					.then(function (returnedUser) {
						console.log(returnedUser.usersID);
						request.login(returnedUser.usersID, function(error) {
							request.session.save(function () {
						      response.redirect('/profile');
							});
						
						});
						
					})
					.catch((error) => {
						console.log("Found the Error!");
						console.log(error);
					})
				});

			});

		} else {
			console.log('passwords don\'t match');
			response.render('account-forms/create-account');
		}
	}
});

router.post('/forgot-password', function(request, response, next) {

	const mailOptions = {
	  from: 'themunchkingame@gmail.com',
	  to: request.body.email,
	  subject: 'Munchkin: Your password has been reset.',
	  text: "Your password wasn't actually reset, we're still testing!"
	};

	transporter.sendMail(mailOptions, function(error, info){
	  if (error) {
	    console.log(error);
	  } else {
	    console.log('Email sent: ' + info.response);
	  }
	}); 

	response.redirect('/forgot-password');
});

passport.serializeUser(function(userID, done) {
	done(null, userID);
});

passport.deserializeUser(function(userID, done) {
	query.findUserByID(userID)
	.then((results)=>{
		done(null, results);
	})
	.catch((error)=>{
		console.log(error);
	})
});

function authenticationMiddleware (redirect) {  
	return function (req, res, next) {

		console.log(`req.session.passport.user: ${JSON.stringify(req.session.passport)}`);

	    if (req.isAuthenticated()) {
	    	return next();
	    }
		console.log('failed auth: ')
	    res.redirect(redirect)
	}
}

module.exports = router; 



