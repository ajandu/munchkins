const express = require('express');
const passport = require('passport');
const router = express.Router();
const pgp = require('pg-promise')();

const insert = require('../db/insert_functions');
const query = require('../db/query_functions');
const database = require('../db/index');


const bcrypt = require('bcrypt');
const saltRounds = 10;

  router.get('/:gameID' ,function(request, response, next){

    var io = request.app.io;
  
    var gameID = request.params["gameID"];
    var userID = parseInt(request.user.usersID);
    var userName = "\'" + request.user.username + "\'";

    response.render("game_board", {gameID : gameID, userID: userID,userName: userName});

  });



module.exports = router;