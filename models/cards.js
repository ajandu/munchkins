'use strict';
module.exports = (sequelize, DataTypes) => {
  var Cards = sequelize.define('Cards', {
    cardID: Sequelize.INTEGER,
    name: DataTypes.TEXT,
    type: DataTypes.INTEGER
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    }
  });
  return Cards;
};