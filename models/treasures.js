import { strictEqual } from "assert";

'use strict';
module.exports = (sequelize, DataTypes) => {
    var Treasures = sequelize.define('Treasures', {
        treasureID: DataType.INTEGER,
        name: DataTypes.TEXT,
        goldAmount: DataTypes.INTEGER,
        cardID_FK: {
            type: Sequelize.INTEGER
        },
        effectID_FK:{
            type: Sequelize.INTEGER
        }
    },
        {
            classMethods: {
                associate: function (models) {
                    // associations can be defined here
                    Treasures.belongsTo(models.Cards);
                    Treasures.belongsTo(models.Player_Effects);
                }
            }
        });
    return Treasures;
};