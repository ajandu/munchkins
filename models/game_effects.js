'use strict';
module.exports = (sequelize, DataTypes) => {
  var Game_Effects = sequelize.define('Game_Effects', {
    id: Sequelize.INTEGER,
    description: Sequelize.TEXT,
    effect: DataTypes.INTEGER
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    }
  });
  return Game_Effects;
};