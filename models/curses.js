'use strict';
module.exports = (sequelize, DataTypes) => {
  var Curses = sequelize.define('Curses', {
    curseID:Sequelize.INTEGER,
    doorID_FK: Sequelize.INTEGER,
    effectID_FK: DataTypes.INTEGER
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
        Curses.belongsTo(models.player_effects);
      }
    }
  });
  return Curses;
};