'use strict';
module.exports = (sequelize, DataTypes) => {
  var Class = sequelize.define('Class', {
    classID: Sequelize.TEXT,
    name: DataTypes.TEXT,
    effect_id: Sequelize.INTEGER,
    door_id: Sequelize.INTEGER
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
        Class.belongsTo(models.player_effects);
        Class.belongsTo(models.doors);
      }
    }
  });
  return Class;
};