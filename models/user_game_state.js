import { strictEqual } from "assert";

'use strict';
module.exports = (sequelize, DataTypes) => {
    var User_Game_State = sequelize.define('User_Game_State', {
        userGameStatesID: {
            type: Sequelize.INTEGER 
        },
        userID_FK: {
            type: Sequelize.INTEGER
        },
        gameID_FK: {
            type: Sequelize.INTEGER
        },
        userHand: {
            type: Sequelize.ARRAY(Sequelize.INTEGER)
        },
        cardsInPlay: {
            type: Sequelize.ARRAY(Sequelize.INTEGER)
        },
        currentUserPhase: {
            type: Sequelize.INTEGER
        },
        userClass: {
            type: Sequelize.INTEGER
        },
        classValue: {
            type: Sequelize.INTEGER
        },
        userEquipment: {
            type: Sequelize.ARRAY(Sequelize.INTEGER)
        },
        equipmentValue: {
            type: Sequelize.INTEGER
        },
        userLevel: {
            type: Sequelize.INTEGER
        },
        userStrength: {
            type: Sequelize.INTEGER
        }
    },
        {
            classMethods: {
                associate: function (models) {
                    // associations can be defined here
                    User_Game_State.belongsTo(models.Users);
                    User_Game_State.belongsTo(models.Games);
                }
            }
        });
    return User_Game_State;
};