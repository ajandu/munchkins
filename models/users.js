'use strict';
module.exports = (sequelize, DataTypes) => {
  var Users = sequelize.define('Users', {
    email: DataTypes.TEXT,
    username: DataTypes.TEXT,
    password: DataTypes.TEXT,
    games_played: DataTypes.INTEGER,
    games_won: DataTypes.INTEGER
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    }
  });
  return Users;
};