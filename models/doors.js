'use strict';
module.exports = (sequelize, DataTypes) => {
  var Doors = sequelize.define('Doors', {
    doorID: DataTypes.INTEGER,
    name: DataTypes.TEXT,
    cardID_FK: {
      type: Sequelize.INTEGER
    }
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
        Doors.belongsTo(models.Cards);
      }
    }
  });
  return Doors;
};