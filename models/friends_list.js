'use strict';
module.exports = (sequelize, DataTypes) => {
  var Friends_List = sequelize.define('Friends_List', {
    user_id: DataTypes.INTEGER,
    friend_id: DataTypes.INTEGER
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    }
  });
  return Friends_List;
};