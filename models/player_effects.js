'use strict';
module.exports = (sequelize, DataTypes) => {
  var Player_Effects = sequelize.define('Player_Effects', {
    playerEffectID: Sequelize.INTEGER,
    description: Sequelize.TEXT,
    effect: DataTypes.INTEGER
  }, {
      classMethods: {
        associate: function (models) {
          // associations can be defined here
        }
      }
    });
  return Player_Effects;
};