import { strictEqual } from "assert";

'use strict';
module.exports = (sequelize, DataTypes) => {
    var Games = sequelize.define('Games', {
        GameID: DataType.INTEGER,
        userList: DataTypes.Array(DataTypes.INTEGER),
        treasureDeck: DataTypes.Array(DataTypes.INTEGER),
        treasureDiscard: DataTypes.Array(DataTypes.INTEGER),
        doorDeck: DataTypes.Array(DataTypes.INTEGER),
        doorDiscard: DataTypes.Array(DataTypes.INTEGER),
        cardsInPlay: DataTypes.Array(DataType.INTEGER),
        currentUserTurn: DataTypes.INTEGER, 
    },
        {
            classMethods: {
                associate: function (models) {
                    // associations can be defined here

                }
            }
        });
    return Games;
};