if (process.env.NODE_ENV === 'development') {
  require("dotenv").config();
}


const express = require('express');
const socket_io = require('socket.io');
var sockethandler = require('./socket/gameSocket')

var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
const expressValidator = require('express-validator');

const database = require('./db/index');
const initializeDB = require('./db/initialize_db');
const userQuery = require('./db/query_functions');

const session = require('express-session');
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const bcrypt = require('bcrypt');

const nodemailer = require('nodemailer');

const transporter = nodemailer.createTransport({
  service: 'gmail',
  host: 'smtp.gmail.com',
  auth: {
    user: process.env.EMAIL_ADDRESS,
    pass: process.env.EMAIL_PASSWORD
  }
});

var index = require('./routes/index');
var db = require('./db/index');
var tests = require('./tests/index');
var game = require('./routes/game');


var app = express();
module.exports = app;

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: false
}));
app.use(expressValidator({
  customValidators: {
    isEqual: (value1, value2) => {
      return value1 === value2
    }
  }
}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use(session({
  store: new(require('connect-pg-simple')(session))(),
  secret: process.env.SESSION_SECRET,
  resave: false,
  saveUninitialized: false
}));
app.use(passport.initialize());
app.use(passport.session());

app.use(function (request, response, next) {
  response.locals.isAuthenticated = request.isAuthenticated()
  next();
});

app.use('/', index);
app.use('/tests', tests);
app.use('/game', game);

passport.use(new LocalStrategy(
  function (username, password, done) {

    const userQuery = require('./db/query_functions');
    console.log('checking passwords');
    userQuery.findUserByUsername(username)
      .then(function (returnedUserObj) {
        console.log(username, '-> Valid username!');

        bcrypt.compare(password, returnedUserObj.password.toString(), function (err, response) {
          if (response === true) {
            console.log('Password correct!')
            return done(null, returnedUserObj.usersID)
          } else {
            console.log('Password incorrect!')
            return done(null, false);
          }
        });

      }).catch(function (errorObject) {

        console.log(errorObject.message);
        console.log(username, '-> Invalid username!');
        return done(null, false);

      });
  }
));


app.use(function (req, res, next) {
  let err = new Error('Not Found');
  err.status = 404;
  next(err);
});

app.use(function (err, req, res, next) {
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  res.status(err.status || 500);
  res.render('error');
});


database.one('SELECT CASE WHEN EXISTS (SELECT * FROM "Cards" LIMIT 1) THEN 1 ELSE 0 END')

  .then((queryResult) => {
    if (queryResult.case === 0) {
      console.log("initializing database...");
      initializeDB.databaseInit();
    } else {
      console.log("Database is already initialized!");
    }
  })
  .catch((error) => {
    console.log("You fudged up!");
    console.log(error);
  });
