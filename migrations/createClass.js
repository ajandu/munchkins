'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Classes', {
      classesID: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      name: {
        type: Sequelize.TEXT
      },
      effectID_FK: {
        type: Sequelize.INTEGER,
        references:{
          model:'Player_Effects',
          key:'playerEffectsID'
        }
      },
      doorID_FK: {
        type: Sequelize.INTEGER,
         references:{
          model:'Doors',
          key:'doorsID'
        }
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Classes');
  }
};