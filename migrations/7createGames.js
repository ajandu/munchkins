'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Games', {
        gamesID: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      userList: {
        type: Sequelize.ARRAY(Sequelize.INTEGER),
        allowNull: false
      },
      treasureDeck: {
        type: Sequelize.ARRAY(Sequelize.INTEGER),
        allowNull: false
      },
      treasureDiscard: {
        type: Sequelize.ARRAY(Sequelize.INTEGER),
        allowNull: false
      },
      doorDeck: {
        type: Sequelize.ARRAY(Sequelize.INTEGER)
      },
      doorDiscard: {
        type: Sequelize.ARRAY(Sequelize.INTEGER)
      },
      cardsInPlay: {
        allowNull: false,
        type: Sequelize.ARRAY(Sequelize.INTEGER)
      },
      currentUserTurn: {
        allowNull: false,
        type: Sequelize.INTEGER
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Games');
  }
};