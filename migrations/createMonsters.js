'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Monsters', {
      monstersID: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      doorID_FK: {
        type: Sequelize.INTEGER,
        references:{
          model:'Doors',
          key:'doorsID'
        }
      },
      strength: {
        type: Sequelize.INTEGER
      },
      rewards: {
        type: Sequelize.INTEGER
      },
      level_up_amount: {
        type: Sequelize.INTEGER
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Monsters');
  }
};