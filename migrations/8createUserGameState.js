'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('User_Game_States', {
      userGameStatesID: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      userID_FK: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
          model: 'Users',
          key: 'usersID'
        }
      },
      gameID_FK: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
          model: 'Games',
          key: 'gamesID'
        }
      },

      userHand: {
        type: Sequelize.ARRAY(Sequelize.INTEGER),
        allowNull: true
      },
      cardsInPlay: {
        //might need to change this to true depending on if it will throw an error
        //at the beginning of the game when no cards are in play
        allowNull: true,
        type: Sequelize.ARRAY(Sequelize.INTEGER)
      },
      currentUserPhase: {
        allowNull: false,
        type: Sequelize.INTEGER
      },
      userClass: {
        //set to true because there could be situations where user never 
        //draw a class card, so the entry in the column might be null
        allowNull: true,
        type: Sequelize.INTEGER
      },
      classValue: {
        //needs to be initialized to 0
        allowNull: false,
        type: Sequelize.INTEGER
      },
      userEquipment: {
        //set to true because the user may have no equipment

        allowNull: true,
        type: Sequelize.ARRAY(Sequelize.INTEGER)
      },
      equipmentValue: {
        //need to set this to 0
        allowNull: false,
        type: Sequelize.INTEGER
      },
      userLevel: {
        //users level will always be something, even if it's just 0
        allowNull: false,
        type: Sequelize.INTEGER
      },
      userStrength: {
        //by default we should probably just let the user have like 5 strength
        allowNull: true,
        type: Sequelize.INTEGER
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('User_Game_States');
  }
};