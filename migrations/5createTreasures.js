'use strict';
module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable('Treasures', {
            treasuresID: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER
            },
            name: {
                type: Sequelize.TEXT
            },
            goldAmount: {
                type: Sequelize.INTEGER
            },
            cardID_FK: {
                type: Sequelize.INTEGER,
                references: {
                    model: 'Cards',
                    key: 'cardsID'
                }
            },
            effectID_FK: {
                type: Sequelize.INTEGER,
                references: {
                    model: 'Player_Effects',
                    key: 'playerEffectsID'
                }
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE
            }
        });
    },
    down: (queryInterface, Sequelize) => {
        return queryInterface.dropTable('Treasures');
    }
};
