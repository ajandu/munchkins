'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Curses', {
      cursesID: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      doorID_FK: {
        type: Sequelize.INTEGER,
        references:{
        model:'Doors',
        key:'doorsID'
        }
      },
      effectID_FK: {
        type: Sequelize.INTEGER,
        references:{
        model:'Player_Effects',
        key:'playerEffectsID'
        }
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Curses');
  }
};