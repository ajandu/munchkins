var socket = io();

var userObj = {
  roomNumber: undefined,
  userID: userID
};

$(function () {
  var $createGame = $('.createGame');
  var $joinGame = $('.joinGame');
  var $roomNumber = undefined;

  $createGame.click(function () {
    sendGame();
  })

  $joinGame.click(function () {
    
    $('.joinGame').replaceWith("<div id ='joinContainer'><input id ='roomNumber'> <button id = 'joinGameButton' >Join Game </button> </div>");
  
  });

  $('.container').on('click', '#joinGameButton', function () {
     userObj.roomNumber = $('#roomNumber').val();
     console.log("Room Number: ", userObj.roomNumber);
     
     if(userObj.roomNumber !== ""){
        document.location.href = "/game/" + userObj.roomNumber;
     }
  })

  function sendGame() {
    console.log(userObj.userID);
    socket.emit('makeGame', userObj);
  };

  $('#chat').submit(() => {
    socket.emit('chat message', $('#m').val());
    $('#m').val('');
    return false;
  });

  socket.on('chat message', (msg) => {
    $('#messages').append($('<li>').text(msg));
  })

  socket.on('gameID', (data) => {
    console.log("Received by client new game id:" + data.gameID);
    window.location.href = "/game/" + data.gameID;
  })

  socket.on('connect', () => {
    console.log("connected to server");
  })
});