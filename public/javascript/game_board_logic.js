let gameObject = {
	userList: []
};

let userStateObject = 0; 

let userEquipment = 0; 

let portraitList = []; 
let handList = [];

let cardsSelected = {};

let equipmentSelected = {}; 


function getUserEquipment () {
	return userEquipment; 
}

function setUserEquipment (newEquipmentList) {
	userEquipment = newEquipmentList; 
}

function setGameObject (newGameObject) {
	gameObject = newGameObject;
	console.log(gameObject);
}

function getGameObject() {
	return gameObject; 
}

function getCardsSelected() {
	return cardsSelected; 
}

function setCardsSelected(newObject) {
	cardsSelected = newObject; 
}

function getEquipmentSelected() {
	return equipmentSelected; 
}

function setEquipmentSelected(newObject) {
	equipmentSelected = newObject;
}

function setHandList (newList) {
	handList = newList; 
}

function getHandList () {
	return handList; 
}

function setUserStateObject (newUserObj) {
	userStateObject = newUserObj;
}

function getUserStateObject () {
	return userStateObject;
}

( function ($){
	$.fn.renderCard = function (card){
		console.log("in renderCard", card);
		$(this).find('.title').html("test title");

	};
})( jQuery );

function calculateCardStrength(strength) {
	if(strength < 5) {
		return {
			titleColor: "#E3F2FD",
			bodyColor: "#90CAF9"
		};
	} else if (strength < 10) {
		return {
			titleColor: "#F3E5F5",
			bodyColor: "#BA68C8"
		};
	} else {
		return {
			titleColor: "#FFCDD2",
			bodyColor: "#EF5350"
		};
	}
}

function populateUserList() {
	userList = gameObject.userList
	if(!!userList){
		userList.forEach( (user) => {
			if(!(userID === user.userID_FK) && user.userHand !== undefined) {
				addPortrait(user.userID_FK, user.username, user.userHand.length, user.userStrength, user.userLevel)
			} else {
				setUserStateObject(user);
			}
		});
		console.log(portraitList);
	} 
}

function refreshSelectedItems() {
	setCardsSelected({});
	setEquipmentSelected({}); 

	$(".equipped-card").css({
		"border-color":"grey",
		"border-width": "2px"
	});

	$(".card").css({
		"border-color":"grey",
		"border-width": "2px"
	});
}



function addPortrait (userID, username, handCount, strength, level) {
	portraitList[userID] = {
		username: username,
		handCount: handCount,
		strength: strength, 
		level: level
	}
}

function getPortraitList() {
	portraitList = portraitList.filter(function( element ) {
		return element !== undefined;
	 });
	 return portraitList;
}

function exportSelectedCards(selectedCards) {
	let exportCards = []; 
	Object.entries(selectedCards).forEach(([key, value]) => {
		exportCards.push(value);
	});
	return exportCards; 
}

function populatePlayerPortraitsDOM() {
	let i = 0; 
	populateUserList();
	portraitCollection = getPortraitList(); 
	$('.player-row').children('.player').each(function () {		
		console.log(portraitCollection[i]);
		if(!!portraitCollection[i]) {
			$(this).find('.name').html(portraitCollection[i].username); 
			$(this).find('.cardsInHand').html("Cards in hand: " + portraitCollection[i].handCount);
			$(this).find('.gold').html("Strength: " + portraitCollection[i].strength);
			$(this).find('.level').html("Level: " + portraitCollection[i].level);
		} else {
			$(this).css('display','none');
		}
		i++; 
	});
}

function populatePlayerStatsDOM() {
	if(getUserStateObject() !== 0) {
		$(".phase").html("Phase: " + getUserStateObject().currentUserPhase);
		$(".userLevel").html("Level: " + getUserStateObject().userLevel); 
		$(".userStrength").html("Strength: " + getUserStateObject().userStrength);
	}
}


function checkIfSelected(cardTitle) {
	if(!!cardsSelected[cardTitle]) return true; 
	
	return false; 
};

function populateHandDOM() {
	let i = 0; 
	let cardList = [];
	
	if(!!userStateObject.userHand) {
		//debugger;
		setHandList(populateHandList(userStateObject.userHand));		
	} else {
		console.log("Hand has not yet been retrieved from server"); 
	}

	$('.cards-in-hand').children('.card').each(function () {
		if(getHandList()[i].title === "empty") {
			$(this).css("display", "none");
		} else {
			$(this).renderCard(getHandList()[i]);
		}
		i++; 
	});
}

function populateCardInPlayDOM() {
	// populating card in play with dummy card object 
	//getGameObject().cardsInPlay.push(getGameObject().userList[0].userHand[1]);
	let cardInPlay = getGameObject().cardsInPlay[0]; 
	console.log("card in play", cardInPlay);

	if(cardInPlay !== undefined){
		console.log(cardInPlay.type)
		$('#card-in-play').renderCard(cardInPlay);		
	}
}

function populateClassDOM() {
	let classInPlay = getGameObject()
}

function populateEquipmentDOM(cardsSelected){
	$('.equipped-card').remove(); 
	//cardsSelected should be an object, right now it's just an array though. 
	// TODO: remove this for in loop and have cardsSelected be an array of objects, not array of ID's 
	let equipment = [];
	for(card in cardsSelected) {
		equipment.push({
			strength: 5, 
			title: 'test name',
			id: cardsSelected[card]
		});
	};
	setUserEquipment(equipment);
	let iterator = 0; 
	for(var equip in getUserEquipment()){

		var prop = equipment[equip]

		$('.equipment').append(`
 		<div class ="equipped-card" id="${iterator}">
 			<div class = "equipped-strength">
 				<h2> ${prop.strength} </h2> 
 			</div> 
 			<div class = "equipped-title"> 
 				<p> ${prop.title} </p> 
 			</div> 
 		</div>
		 `);
		 iterator++; 
	}
}

function populateHandList(cardList) {
	console.log("card list", cardList)
	let defaultHand = []
	console.log("cardList length", cardList.length)

	cardList.forEach((card) => {
		cardObject = {
			cardID: card.cardsID,
			name: card.name,
			type: card.type,
			strength: card.strength,
			description: card.description,
			goldAmount: card.goldAmount,
			rewards: card.rewards,
			level_up_amount: card.level_up_amount //Math.floor(Math.random() * (10 - 1) + 1)
		}

		defaultHand.push(cardObject);
	}) 

	if(cardList.length < 10){
		emptyCard = {
			cardID: 0,
			title: 'empty',
			type: 0,
			strength: 0
		}

		for(i = 0; i < 10 - cardList.length; i++){
			defaultHand.push(emptyCard);
		}
	}
	return defaultHand; 
}

$(function () {
	
	socket.emit('getUpdate', {
		userName: userName,
		roomNumber: roomNumber,
		userID: userID,
	});

    $('#phase-one').click(() => {
      console.log('trying to start game');
      socket.emit('phaseOne',{
        userName: userName,
        roomNumber: roomNumber,
        userID: userID
      })
	})

})

	$('.cards-in-hand').on('click', '.card', function(){ 
		if(handList.length != 0) {
			if( checkIfSelected( $(this).closest("div").prop("id") ) ){

				delete cardsSelected[ $(this).closest("div").prop("id") ];
				$(this).css('border-color', 'grey');
				$(this).css('border-width', '2px');
			} else {

				cardsSelected[ $(this).closest("div").prop("id") ] = handList[ $(this).closest("div").prop("id") ];
				$(this).css('border-color', 'green');
				$(this).css('border-width', '.1em');

			}

			console.log('cards selected', cardsSelected);
		}
	});

	$('.equipment').on('click', '.equipped-card', function() {
		if(getUserEquipment().length != 0) {
			if( !!equipmentSelected[ $(this).closest("div").prop("id") ] ) {
				delete equipmentSelected [ $(this).closest("div").prop("id") ];
				$(this).css('border-color', 'grey');
				$(this).css('border-width', '2px');
			} else {
				equipmentSelected[ $(this).closest("div").prop("id") ] = getUserEquipment()[ $(this).closest("div").prop("id") ]
				$(this).css('border-color', 'green');
				$(this).css('border-width', '0.1em');
			}
		}

		console.log(equipmentSelected);
	}); 
  
( function ($){
	$.fn.renderCard = function (card){
		console.log("in renderCard", card);

		switch(card.type){
			case 0:
					//equipment
					$(this).css("display", "show");
					$(this).find('.title').html(card.name); 
					$(this).find('.type-icon').html(`<img src= "../images/equipment-icon.png" class = "icon">`)
					$(this).find('.description').html("Lose a level and one piece of equipment");
					$(this).find('.strength').css("display", "none");
					$(this).find('.rewards-and-level').html(`
						<img src= "../images/coin-icon.png" class = "icon"> 
						<p class ="rewards"> ${card.goldAmount} </p>
						`);
					break;
				case 1:
					//class
					$(this).css("display", "show");
					$(this).find('.title').html(card.name);
					$(this).find('.type-icon').html(`<img src= "../images/class-icon.png" class = "icon">`)
					$(this).find('.description').html("Lose a level and one piece of equipment");
					$(this).find('.cardStrength').html(card.strength);
					$(this).find('.rewards-and-level').css("display", "none");
					$(this).find('.strength').css("display", "none");
					break;
				case 3:
					//monster
					$(this).css("display", "show");
					$(this).find('.title').html(card.name); 
					$(this).find('.type-icon').html(`<img src= "../images/monster-icon.png" class = "icon">`)
					$(this).find('.description').html("Lose a level and one piece of equipment");
					$(this).find('.strength').html(`
						<img src = "../images/strength-icon.png" class = "icon>
						<p class ="cardStrength"> ${card.strength} </p>	
					`)
					$(this).find('.rewards-and-level').html(`
						<img src= "../images/treasure-icon.png" class = "icon"> 
						<p class ="rewards"> ${card.rewards} </p> 
						<img src= "../images/level-up-icon.png" class = "icon"> 
						<p class ="rewards"> ${card.level_up_amount} </p>
						`);
					break;				
				case 4:
					//curse
					$(this).css("display", "show");
					$(this).find('.title').html(card.name); 
					$(this).find('.type-icon').html(`<img src= "../images/curse-icon.png" class = "icon">`)
					$(this).find('.description').html("Lose a level and one piece of equipment");
					$(this).find('.cardStrength').html(card.strength);
					$(this).find('.rewards-and-level').css("display", "none");
					$(this).find('.strength').css("display", "none");
					break;
				default:
					$(this).css("display", "show");
					$(this).find('.title').html("default"); 
					$(this).find('.description').html("Default");
					break;
				break;
		}

	};
})( jQuery );

