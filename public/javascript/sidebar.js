var socket = io.connect();

socket.on('connect', () => {
	console.log("Connected to server!!");
	console.log("room number:  " + roomNumber);
});

socket.on('newMessage', (messageObj) => {
	if (roomNumber === messageObj.roomNumber) {

		console.log("message received:" + messageObj.message);
		console.log("message from ", messageObj.messageUserID);
		console.log("i am userID", userID);
		//$('#messages').append($('<p>').text(messageObj.userName + ' : ' + messageObj.message));

		if(messageObj.messageUserID === userID){
			$('#messages').append(`
			<div class = "own-message">
				<p class ="self">${messageObj.userName}</h5>
				<p class = "own-content">${messageObj.message}</p>
			</div>
		`)

		}else{
			$('#messages').append(`
			<div class = "message">
				<p class ="user">${messageObj.userName}</h5>
				<p class = "content">${messageObj.message}</p>
			</div>
		`)

		}
		
	}
});

//returnedGameObj you can find in the browser by saying getGameObject();
//it'll show you all the shit 
socket.on('sendUpdate', (returnedGameObj) => {
	if (roomNumber === returnedGameObj.gamesID) {
		console.log(returnedGameObj, {
			depth: null
		});
	
		setGameObject(returnedGameObj);
		populatePlayerPortraitsDOM();
		populateHandDOM();
		populatePlayerStatsDOM();
		populateCardInPlayDOM();
		populateClassDOM();
	}
});

// socket.on('sendUpdateEquipment', (returnedGameObj) => {
// 	if(roomNumber === returnedGameObj.gamesID) {
// 		console.log(returnedGameObj, {
// 			depth: null
// 		});
// 		console.log("In send update equipment");
// 		setGameObject(returnedGameObj);
// 		populatePlayerPortraitsDOM();
// 		populateHandDOM();
// 		populatePlayerStatsDOM();
// 		populateEquipmentDOM(getUserStateObject().userEquipment);
// 		populateCardInPlayDOM();
		
// 	}
// })

socket.on('forceDiscard', (returnedGameObj) => {
	console.log('need to discard hand, to get to 5 cards');
});


$(function () {

	$('#start-game').click(() => {
		console.log('trying to start game');
	
		socket.emit('initializeGame',{
			userName: userName,
			roomNumber: roomNumber,
			userID: userID,
		});
	});

	$('#chat').submit((button) => {
		button.preventDefault();

		console.log("value:" + $('#message').val());

		if ($('#message').length > 0) {

			socket.emit('createMessage', {
				userName: userName,
				roomNumber: roomNumber,
				userID: userID,
				message: $('#message').val()
			});

			console.log($('#message').length);
		}

		$('#message').val('');
		return false;
	});

$('#btn-update').click(() => {
	console.log('asked for update');
	socket.emit('getUpdate', {
		userName: userName,
		roomNumber: roomNumber,
		userID: userID,
	});
});

$('#join-game').click(() => {
	console.log('asked to join game');
	socket.emit('join', {
		userName: userName,
		roomNumber: roomNumber,
		userID: userID,
	});
});

$('.btn-equip').click(() => {
	
	let exportCards = exportSelectedCards(cardsSelected);
	
	let gameObj = {
		userName: userName,
		roomNumber: roomNumber,
		userID: userID,
		cardsSelected: exportCards
	};

	if (!Object.keys(cardsSelected).length > 0) {
		alert('Hey! you need to select some cards to do that!');
	} else {
		console.log(gameObj);
		socket.emit('equip', gameObj);
	}
	console.log(Object.keys(cardsSelected).length);
	refreshSelectedItems(); 

});

$('.btn-discard').click(() => {

	let exportCards = exportSelectedCards(cardsSelected);
	
	let gameObj = {
		userName: userName,
		roomNumber: roomNumber,
		userID: userID,
		cardsSelected: exportCards
	};

	if (!Object.keys(cardsSelected).length > 0) {
		alert('Hey! you need to select some cards to do that!');
	} else {
		console.log(gameObj);
		socket.emit('discard', gameObj);
	}

	console.log(Object.keys(cardsSelected).length);
	refreshSelectedItems(); 
	
});

$('.btn-sell').click(() => {

	let gameObj = {
		userName: userName,
		roomNumber: roomNumber,
		userID: userID,
		cardsSelected: cardsSelected
	};

	if (!Object.keys(cardsSelected).length > 0) {
		alert('Hey! you need to select some cards to do that!');
	} else {
		console.log(gameObj);
		socket.emit('sell', gameObj);
	}

	console.log(Object.keys(cardsSelected).length);

});

$('.btn-fight').click(() => {
	console.log('clicked fight');
	let gameObj = {
		userName: userName,
		roomNumber: roomNumber,
		userID: userID,
		cardsInPlay: gameObject.cardsInPlay
	};

	socket.emit('fight', gameObj);
});

$('.btn-run').click(() => {
	console.log('clicked run');
	let gameObj = {
		userName: userName,
		roomNumber: roomNumber,
		userID: userID,
		cardsInPlay: gameObject.cardsInPlay
	};

	socket.emit('run', gameObj);
});

})
