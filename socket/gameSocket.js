const Server = require('socket.io')
const userQuery = require('../db/query_functions');
const gameState = require('../db/games');
const userGameState = require('../db/user_game_state')
const phaseInit = require('../engine_manager/phaseInit');
const phaseZero = require('../engine_manager/phaseZero');
const phaseOne = require('../engine_manager/phaseOne');
const ngine = require('../engine_manager/nginFunction');
const cards = require('../db/cards');


// Below is a reference to follow when passing the gameObject, or gameObj for short. 
// var gameObject = {
//       userList: [], // add the current user here NEED TO BE HERE OR IT SHOOTS NULL SHIT. 
//       treasureDeck: [],
//       treasureDiscard: [],
//       doorDeck: [],
//       doorDiscard: [],
//       cardsInPlay: [],
//       currentUserTurn: 1
// }

module.exports = function (app) {
  const io = Server(app);
  io.on('connection', (socket) => {
    console.log('user has connected: ' + socket.id);

    console.log(socket.handshake.headers.referer, {
      details: null
    });
    socket.join(socket.handshake.headers.host.referer);
    socketHandler.createMessage(socket, {
      userName: ' ',
      message: 'Someone joined the room'
    });

    socket.on('disconnect', () => {
      console.log('user has left:' + socket.id);
      socketHandler.createMessage(socket, {
        userName: ' ',
        message: 'Someone Left!'
      });
    });

    socket.on('makeGame', (params) => {
      console.log('attempting to make a game: ' + params);
      socketHandler.makeGame(socket, params);
    });

    socket.on('join', (gameObj, currentID) => {

      // for(userIndex in gameObj.userList) {

      //   let user = gameObj.userList[userIndex];

      //   if(user.usersID === currentID) {
      //     return 'User already exists';
      //   }

      // }

      socketHandler.join(socket, gameObj);
    });

    socket.on('initializeGame', (gameObj) => {
      socketHandler.initialize(socket, gameObj);
    });

    socket.on('createMessage', (messageObj) => {
      socketHandler.createMessage(socket, messageObj);
    });


    // userGameState.addEquipmentCard = function (cardPK, usersID,gameID)
    socket.on('equip', (gameObj) => {
      //gameObj has the array of equipment cards in it,
      //send it over to ngine.equipCards() which will then add the cards to the 
      //userEquipment 
      ngine.equipCards(gameObj)
        .then((result) => {
          socketHandler.getUpdate(socket, gameObj);
        })
        .catch((error) => {
          console.log(error);
        })
    });

    socket.on('sell', (gameObj) => {
      console.log('Selling: ', gameObj.cardsSelected);
    });

    socket.on('unequip', (gameObj) => {
      console.log('Unequipping: ', gameObj.cardsSelected);
    });

    socket.on('phaseOne', (gameObj) => {
      socketHandler.phaseOne(socket, gameObj);
    });

    socket.on('discard', (gameObj) => {
      for (element in gameObj.cardsSelected) {
        var card = gameObj.cardsSelected[element]
        console.log("Discarding card", card, "from game", gameObj.roomNumber, "from player", gameObj.userID);
        userGameState.removeCardFromHand(card.cardID, gameObj.userID, gameObj.roomNumber);
      }
      socketHandler.getUpdate(socket, gameObj);

      console.log('Discarding: ', gameObj.cardsSelected);
    });

    socket.on('run', (gameObj) => {
      socketHandler.run(socket, gameObj);
    });

    socket.on('fight', (gameObj) => {
      console.log(gameObj.userName + "Entering Fight!");
      socketHandler.fightMonster(socket, gameObj);
    });

    socket.on('getUpdate', (gameObj) => {
      socketHandler.getUpdate(socket, gameObj);
    });

    socket.on('processCardInPlay', (gameObj) => {
      console.log("preparing to process card in play....");
      socketHandler.processCardInPlay(gameObj)
        .then((result) => {
          ngine.updateStrength(gameObj);
        })
        .catch((error) => {
          console.log(error);
        });
    });


  });


  var socketHandler = {

    makeGame: function (socket, gameObj) {

      console.log('userID ' + gameObj.userName + ' is attempting to make a game');
      var gameObject = {
        userList: [],
        treasureDeck: [],
        treasureDiscard: [],
        doorDeck: [],
        doorDiscard: [],
        cardsInPlay: [],
        currentUserTurn: 1
      }

      gameState.addGame(gameObject)
        .then((newGameID) => {
          gameState.insertUser(gameObj.userID, gameObj.roomNumber)
            .then((results) => {
              socket.emit('gameID', {
                gameID: newGameID
              })
            })
            .catch((error) => {
              console.log(error)
            })
          console.log("Newly created room joining room: " + newGameID);
        })
        .catch((error) => {
          console.log(error)
        })
    },

    join: function (socket, gameObj) {

      console.log("connecting to : " + gameObj.roomNumber);
      var room = gameObj.roomNumber;

      ngine.userCheckandInsert(gameObj)
        .then((value) => {
          if (value === true) {

            socket.emit('gameID', {
              gameID: gameObj.roomNumber
            });
            console.log('here is a userName: ' + gameObj.userName);
            io.in(socket.handshake.headers.host.referer).emit('newMessage', {
              userName: gameObj.userName,
              message: 'joined ' + gameObj.roomNumber,
            });
          } else {
            console.log('User ' + gameObj.userName + ' in unable to join the game! there is no space!');
            io.in(socket.handshake.headers.host.referer).emit('newMessage', {
              userName: gameObj.userName,
              message: 'unable to add to game, no space',
            });
          }
        })
        .catch(error => {
          console.log(error)
        })
    },

    createMessage: (socket, messageObj) => {
      console.log("in create message");
      console.log(messageObj.message);
      console.log("Want to emit to this room: " + messageObj.roomNumber);
      console.log("userID in createmessage", messageObj.userID);
      io.in(socket.handshake.headers.host.referer).emit('newMessage', {
        userName: messageObj.userName,
        message: messageObj.message,
        messageUserID: messageObj.userID,
        roomNumber: messageObj.roomNumber
      });
    },

    initialize: (socket, gameObj) => {
      console.log("Attempting start phase");
      phaseInit.startPhase(socket, gameObj);
    },

    phaseOne: (socket, gameObj) => {
      phaseOne.startPhase(socket, gameObj)
        .then((startPhaseResult) => {
          if (startPhaseResult.type === 3) {
            ngine.updateStrength(gameObj)
              .then(() => {
                console.log("STRENGTH UPDATED AFTER CARD PROCESSED");
              })
              .catch((error) => {
                console.log(error);
              })
          } else {
            phaseOne.processCardInPlay(socket, gameObj)
              .then((processResult) => {
                ngine.updateStrength(gameObj)
                  .then(() => {
                    console.log("STRENGTH UPDATED AFTER CARD PROCESSED");
                  })
                  .catch((error) => {
                    console.log(error);
                  })

              })
              .catch((error) => {
                console.log(error);
              });
          }
        })
        .catch((error) => {
          console.log(error);
        })
    },

    // getUpdatedCardsInPlay: (socket, gameObj) => {
    //   ngine.getGameState(socket, gameObj)
    //   .then((returnedGameObj) => {
    //     Promise.all(returnedGameObj.cardsInPlay.map(cards => { 
    //right here there needs to be a different function for 
    //getIdentifiedCards because cardsInPlay is not user.cardsInPlay, it's just
    //called cardsInPlay 
    //       return ngine.getIdentifiedCards(cards)
    //       .then(identifiedCards => user.userEquipment = identifiedCards)
    //     }))
    //     .then((results) => {
    //       io.in(socket.handshake.headers.host.referer).emit('sendCardsInPlay', returnedGameObj);
    //     })
    //   })
    //   .catch((error) => {
    //     console.log(error);
    //   }); //end ngine.getGameState(socket, gameObj)
    // },

    //this is for updating the equipment
    // getUpdateEquipment: (socket, gameObj) => {
    //   ngine.getGameState(socket, gameObj)
    //     .then((returnedGameObj) => {
    //       Promise.all(returnedGameObj.userList.map(user => {
    //           return ngine.getEquipmentCards(user)
    //             .then(identifiedCards => user.userEquipment = identifiedCards)
    //         }))
    //         .then((results) => {
    //           io.in(socket.handshake.headers.host.referer).emit('sendUpdateEquipment', returnedGameObj);
    //         })
    //     })
    //     .catch((error) => {
    //       console.log(error);
    //     }); //end ngine.getGameState(socket, gameObj)
    // },

    //this is just for updating the hand
    getUpdate: (socket, gameObj) => {
      ngine.getGameState(socket, gameObj)
        .then((returnedGameObj) => {
          const localGameObj = returnedGameObj;
          ngine.getCardObject(localGameObj.cardsInPlay)
            .then(results => localGameObj.cardsInPlay = results)

          Promise.all(
              [
                Promise.all(localGameObj.userList.map(user => {
                  return ngine.getIdentifiedCards(user)
                    .then(identifiedCards => user.userHand = identifiedCards)
                })),
                Promise.all(localGameObj.userList.map(user => {
                  if (user.userEquipment !== null) {
                    return ngine.getEquipmentCards(user)
                      .then(equipmentCards => user.userEquipment = equipmentCards)
                  } else {
                    return null
                  }
                })),
                Promise.all(localGameObj.userList.map(user => {
                  if (user.cardsInPlay !== null) {
                    return ngine.getCardsInPlayObject(user)
                      .then(returnedCards => user.cardsInPlay = returnedCards)
                  } else {
                    return null
                  }
                })),
                Promise.all(localGameObj.userList.map(user => {
                  if (user.userClass !== null) {
                    return ngine.getClassObject(user)
                      .then(returnedClass => user.userClass = returnedClass)
                  } else {
                    return null
                  }
                })),
              ]
            )
            .then((results) => {
              io.in(socket.handshake.headers.host.referer).emit('sendUpdate', localGameObj);
            })
        })
        .catch((error) => {
          console.log(error);
        }); //end ngine.getGameState(socket, gameObj)
    },

    checkHand: (socket, gameObj) => {
      ngine.checkHand(gameObj)
        .then((result) => {
          if (results === true) {} else {
            socketHandler.getUpdate(socket, gameObj);
            io.in(socket.handshake.headers.host.referer).emit('forceDiscard', gameObj);
          }
        })
    },

    // equipItems: (socket, gameObj) => {
    //   phaseZero.equipCards(socket, gameObj);
    // },

    fightMonster: (socket, gameObj) => {
      //I think here we need make the 
      console.log("here are the contents of the game Object " + Object.getOwnPropertyNames(gameObj));
      phaseOne.fightMonster(socket, gameObj);
      //should wrap fight monster in a promise so after it resolves it goes 
      //to phase 3 
    },

    run: (socket, gameObj) => {
      phaseOne.runFromMonster(socket, gameObj);
      //from here you should have runFromMonster return a number
      //if the number is 5 or 6 go to phase2, it should go to fight monster 
    },

    processCardInPlay: (socket, gameObj) => {
      phaseOne.processCardInPlay(socket, gameObj);
      //make sure when this promise resolves, we go start phase 2 
    }
  }
}