const userGameState = require('../db/user_game_state');
const gamesTable = require('../db/games');
const cards = require('../db/cards');
const monsters = require('../db/monsters');
const curses = require('../db/curses');

module.exports = {
  startPhase: function (socket, gameObj) {
    
    return new Promise ((resolve, reject) => {
      console.log('PhaseONE!: GameID : ' + gameObj.roomNumber)
      gamesTable.readDoorDeck(gameObj.roomNumber)
        .then((resultFromDoorDeck) => {
          let cardFromDoorDeck = resultFromDoorDeck.doorDeck.pop();
          console.log('playing card: '+ cardFromDoorDeck); 
          gamesTable.addCardInPlay(cardFromDoorDeck, gameObj.roomNumber)
            .then((resultCardInPlay) => {
              console.log(resultCardInPlay); 
                gamesTable.dealCardFromDoorDeck(resultCardInPlay.cardsInPlay[0], gameObj.roomNumber)
                .then((resultDealtCard) => {
                  console.log("card removed from door deck");
                  cards.readCard(resultCardInPlay.cardsInPlay[0])
                  .then((card) => {
                    resolve(card);
                  })
                  .catch((error) => {
                    console.log(error);
                  })
                })
                .catch((error) => {
                  console.log(error);
                });
            })
            .catch(error => {
              console.log(error)
            })
        })
        .catch(error => {
          console.log(error)
        })
    })
  },

  processCardInPlay: function(socket, gameObj) {
    return new Promise((resolve, reject) => {
      gamesTable.readCardsInPlay(gameObj.roomNumber)
      .then((cardInPlayResult) => {
        cards.readCard(cardInPlayResult.cardsInPlay[0])
        .then((cardResult) => {
          if( cardResult.type === 3 ) {
            resolve(cardResult);
          } else if( cardResult.type === 4 ) {
            curses.readCurseCard(cardResult.cardsID)
            .then((curseResult) => {
              if (curseResult.effect === 0) {
                console.log("Applying lose equipment Curse");
                applyLoseEquipmentCurse(gameObj.userID, gameObj.roomNumber);
                resolve (curseResult);
              } else if (curseResult.effect === 1) {
                console.log("Applying lose level curse");
                applyLoseLevelCurse(gameObj.userID, gameObj.roomNumber);
                resolve(curseResult);
              } else if (curseResult.effect === 12) {
                console.log("Applying lose level and lose equipment curse");
                applyLoseEquipmentCurse(gameObj.userID, gameObj.roomNumber);
                applyLoseLevelCurse(gameObj.userID, gameObj.roomNumber);
                resolve(curseResult);
              }
              else {
                console.log("Applying skip that turn curse, don't forget to implement this!");
                resolve(curseResult);
              }
            }) 
            .catch((error) => {
              console.log(error);
            })
          } else {
            console.log("adding card to hand:");
            userGameState.addCardToHand(cardResult.cardsID, gameObj.userID, gameObj.roomNumber);
            resolve(cardResult);
          } 

        }) 
        .catch((error) => {
          console.log(error);
        })
      }) 
      .catch((error) => {
        console.log(error);
      }) 
    })
  },

  fightMonster: function(socket, gameObj) {
    monsters.readMonster(gameObj.cardsID)
    .then((monsterResult) => {
      userGameState.getUserStrength(gameObj.userID, gameObj.roomNumber)
      .then((resultStrength) => {
        if (resultStrength.userStrength >= monsterResult.strength) {
          console.log("you won the fight!");
          applyLevelUpAmount(gameObj.userID, monsterResult.level_up_amount, gameObj.roomNumber);
          applyRewardsAmount(gameObj.userID, monsterResult.rewards, gameObj.roomNumber);
        } else {
          console.log("You got fucked up!");
          lostFightAgainstMonster(gameObj.userID, gameObj.roomNumber);
        };
      })
      .catch((error) => {
        console.log(error);
      });
    })
    .catch((error) => {
      console.log(error);
    })
  },

  runFromMonster: function(socket, gameObj) {
    const diceRoll = getDiceRollValue(1,6);
    console.log(diceRoll); 
    if( diceRoll === 6 || diceRoll === 5 ) {
      console.log("Got Away Safely!!");
    } else {
      console.log("forced into the fight!");
      forcedToFightMonster(gameObj.userID, gameObj.cardID, gameObj.roomNumber);
    }
  },
}

applyLoseEquipmentCurse = function (userID, roomNumber) {
  userGameState.getEquipmentCards(userID, roomNumber)
    .then((equipmentCards) => {
      if(equipmentCards.userEquipment !== null){
        equipmentCards.userEquipment.pop();
      }
      let currentEquipmentCards = equipmentCards.userEquipment;
      let myequipment = equipmentCards.userEquipment;
      console.log("Current Equipment: " + myequipment);
      console.log("Array being passed in: " + myequipment);
      userGameState.updateEquipmentCards(myequipment, equipmentCards.userID_FK, roomNumber)
        .then((results) => {
          userGameState.updateEquipmentValue(equipmentCards.userID_FK, roomNumber);
        })
        .catch((error) => {
          console.log(error);
        });
    })
    .catch((error) => {
      console.log(error);
    }); 
};

applyLoseLevelCurse = function (userID, roomNumber) {
  userGameState.getUserLevel(userID, roomNumber)
    .then((resultUserLevel) => {
      let updatedLevel = resultUserLevel.userLevel - 1;
      userGameState.updateUserLevel(updatedLevel, userID, roomNumber);
    })
    .catch((error) => {
      console.log(error);
    });
};

applySkipTurnCurse = function (userID, roomNumber) {

};



lostFightAgainstMonster = function (usersID, roomNumber) {
  userGameState.getUserLevel(usersID, roomNumber)
    .then((resultLevel) => {
      let updatedLevel = resultLevel.userLevel - 1;
      userGameState.updateUserLevel(updatedLevel, usersID, roomNumber);
    })
    .catch((error) => {
      console.log(error);
    });
};

applyLevelUpAmount = function (userID, levelUpAmount, roomNumber) {
  userGameState.getUserLevel(userID, roomNumber)
    .then((resultLevel) => {
      let updatedUserLevel = resultLevel.userLevel + levelUpAmount;
      userGameState.updateUserLevel(updatedUserLevel, userID, roomNumber);
    })
    .catch((error) => {
      console.log(error);
    })
};

applyRewardsAmount = function (userID, rewards, roomNumber){
  return Promise.resolve().then(() => {
    return Promise.all([
      gamesTable.readTreasureDeck(roomNumber),
    ])
  }).then(([{ treasureDeck }]) => {
    return Promise.all(Array.from({ length: rewards }, () => {
      const dealTreasureCard = treasureDeck.pop()
      return Promise.all([
        userGameState.addCardToHand(dealTreasureCard, userID, roomNumber),
        gamesTable.dealCardFromTreasureDeck(dealTreasureCard, roomNumber),
      ])
    }))
  })
};

removeCardFromCardsInPlay = function( cardPK, gamesID ) {
  gamesTable.removeCardInPlay( cardPK, gamesID );
};

getDiceRollValue = function (min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min; 
};

forcedToFightMonster = function(userID, cardID, roomNumber) {
  monsters.readMonster(cardID)
  .then((monsterResult) => {
    userGameState.getUserStrength(userID, roomNumber)
    .then((resultStrength) => {
      if (resultStrength.userStrength >= monsterResult.strength) {
        console.log("you won the fight!");
        applyLevelUpAmount(userID, monsterResult.level_up_amount, roomNumber);
        applyRewardsAmount(userID, monsterResult.rewards, roomNumber);
      } else {
        console.log("You got fucked up!");
        lostFightAgainstMonster(userID, roomNumber);
      };
    })
    .catch((error) => {
      console.log(error);
    });
  })
  .catch((error) => {
    console.log(error);
  })
};


