const gameState = require('../db/games')
const userGameState = require('../db/user_game_state')
const cardTable = require('../db/cards')

module.exports = {
  shuffleDecks: function (deck) {
    let shuffledDeck = deck;

    for (let index = shuffledDeck.length - 1; index >= 0; index--) {

      var randomIndex = Math.floor(Math.random() * (index + 1));
      var itemAtIndex = shuffledDeck[randomIndex];

      shuffledDeck[randomIndex] = shuffledDeck[index];
      shuffledDeck[index] = itemAtIndex;
    }
    return shuffledDeck;
  },

  fillRange: (start, end) => {
    return Array(end - start + 1).fill().map((item, index) => start + index);
  },

  userCheckandInsert: function (gameObj) {
    console.log('trying to add userID ' + gameObj.userID)
    return new Promise(
      function (resolve, reject) {
        gameState.getUsers(gameObj.roomNumber)
          .then((results) => {

              if (results.userList.indexOf(gameObj.userID) === -1) {
                if (results.userList.length === 4) {
                  console.log('game full cannot add: ' + gameObj.roomNumber + ' User: ' + gameObj.userID);
                  resolve(false);
                } else {

                  console.log('user ' + gameObj.userID + 'is able to join, adding now')
                  gameState.insertUser(gameObj.userID, gameObj.roomNumber)
                    .then((results) => {
                      resolve(true);
                    })
                    .catch((error) => {
                      console.log(error);
                      reject(error);
                    })
                }
              } else {

                console.log('user ' + gameObj.userID + ' was already in the game')
                resolve(true);
              }
            }

          )
      });
  },

  //done, it gives them 
  //returnGameObject, has all game states and has all user states
  //
  // 	getGameState: function (socket, gameObj) {
  // 		return new Promise(
  // 				function (resolve, reject) {

  // 						gameState.getGameState(gameObj.roomNumber)
  // 								.then((returnedGameObject) => {
  // 										userGameState.getAllUsersInGame(gameObj.roomNumber)
  // 												.then((results) => {
  // 														returnedGameObject.userList = results;
  // 														resolve(returnedGameObject);
  // 												})
  // 								})
  // 				});
  // },

  //now that we have the userList we can get each usersPK 
  //
  getGameState: function (socket, gameObj) {
    return new Promise(
      function (resolve, reject) {
        gameState.getGameState(gameObj.roomNumber)
          .then((returnedGameObject) => {
            userGameState.getAllUsersInGame(gameObj.roomNumber)
              .then((results) => {
                returnedGameObject.userList = results;
                resolve(returnedGameObject);
              })
              .catch((error) => {
                console.log(error);
              });
          })
          .catch((error) => {
            console.log(error);
          });
      });
  },

  //pretty sure we're not using this 
  getUserHand: function (socket, gameObj) {
    Promise.all(gameObj.userList.map((user) => {
      Promise.all(user.userHand.map((cards) => {
        return cardTable.getAllCardAttributes(cards)
      }))
    }))
  },

  getIdentifiedCards: function (user) {
    return Promise.all(user.userHand.map((cards) => {
      return cardTable.getAllCardAttributes(cards)
    }))
  },

  getEquipmentCards: function (user) {
    return Promise.all(user.userEquipment.map((cards) => {
      return cardTable.getAllCardAttributes(cards)
    }))
  },

  getCardsInPlayObject: function (user) {
    return Promise.all(user.cardsInPlay.map((cards) => {
      return cardTable.getAllCardAttributes(cards)
    }))
  },
  getClassObject: function (user) {
    return cardTable.getAllCardAttributes(user.userClass)
  },
  getCardObject: function (cardArray) {
    return Promise.all(cardArray.map((cards) => {
      return cardTable.getAllCardAttributes(cards)
    }))

  },

  setNextUser: function (socket, gameObj) {

    return new Promise(function (resolve, reject) {
      gameState.getGameState(gameObj.roomNumber)
        .then((results) => {
          var currentUserTurnIndex = results.userList.getIndexOf(results.currentUserTurn);
          if (currentUserTurnIndex === results.userList.length) {
            gameState.updateCurrentUserTurn(results.userList[0], gameObj.roomNumber)
              .then((results) => {
                resolve(true)
              })
          } else {
            gameState.updateCurrentUserTurn(results.userList[currentUserTurnIndex + 1], gameObj.roomNumber)
              .then((results) => {
                resolve(true)
              })
          }
        })
    })
  },

  updateStrength: function (gameObj) {
    return new Promise((resolve, reject) => {
      userGameState.updateUserStrength(gameObj.userID, gameObj.roomNumber)
        .then((result) => {
          resolve(result);
        })
        .catch((error) => {
          console.log(error);
        });
    })
  },

  equipCards: function (gameObj) {
    console.log(gameObj, {
      depth: null
    });
    return Promise.all(gameObj.cardsSelected.map((card) => {
      if (card.type === 0) {
        return Promise.resolve().then(() => {
          return Promise.all([
              userGameState.addEquipmentCard(card.cardID, gameObj.userID, gameObj.roomNumber),
              userGameState.removeCardFromHand(card.cardID, gameObj.userID, gameObj.roomNumber)
            ])
            .then(() => {
              return userGameState.updateEquipmentValue(gameObj.userID, gameObj.roomNumber)
            })
            .then(() => {
              return userGameState.updateUserStrength(gameObj.userID, gameObj.roomNumber)
            })
        })
      } else if (card.type === 1) {
        return Promise.resolve().then(() => {
          return Promise.all([
              userGameState.setClassValue(card.cardID, gameObj.userID, gameObj.roomNumber),
              userGameState.removeCardFromHand(card.cardID, gameObj.userID, gameObj.roomNumber)
            ])
            .then(() => {
              return userGameState.updateClassValue(card.cardID, gameObj.userId, gameObj.roomNumber)
            })
        })
      } else if (card.type === 2) {
        return Promise.resolve().then(() => {
          return userGameState.getUserLevel(gameObj.userID,gameObj.roomNumber)
          .then((returnedUserLevel) => {
            if( returnedUserLevel < 9 ){
              return userGameState.updateUserLevel(returnedUserLevel+1,gameObj.userID,gameObj.roomNumber)
                .then(() =>{
                  return userGameState.removeCardFromHand(card.cardID,gameObj.userID,gameObj.roomNumber)
                })
            }
          })
        })
      }
      else if(card.type === 3){
        return Promise.resolve() // constraint to keep people from playing monsters
      }
      else if(card.type === 4){
        

      }

    }))
  },
};