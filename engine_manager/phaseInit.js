const ngine = require('./nginFunction');
const userGameState = require('../db/user_game_state');
const gamesTable = require('../db/games');

module.exports = {
  startPhase: (socket, gameObj) => {
    const doors = ngine.shuffleDecks(ngine.fillRange(1, 75));
    const treasures = ngine.shuffleDecks(ngine.fillRange(76, 155));

    return Promise.resolve().then(() => {
      return Promise.all([
        gamesTable.updateDoorDeck(doors, gameObj.roomNumber),
        gamesTable.updateTreasureDeck(treasures, gameObj.roomNumber),
      ])
    }).then(() => {
      return gamesTable.getUsers(gameObj.roomNumber)
    }).then(({ userList }) => {
      return Promise.all(userList.map((ul) => {
        const newUserState = {
          userID_FK: ul,
          gameID_FK: gameObj.roomNumber,
          currentUserPhase: 0,
          userLevel: 0,
          classValue: 0,
          equipmentValue: 0,
          userStrength: 0,
        }
        return Promise.resolve().then(() => {
          return userGameState.addUserGameState(newUserState)
        }).then((result) => {
          return Promise.all([
            gamesTable.readDoorDeck(gameObj.roomNumber),
            gamesTable.readTreasureDeck(gameObj.roomNumber),
          ])
        }).then(([{ doorDeck }, { treasureDeck }]) => {
          return Promise.all(Array.from({ length: 5 }, () => {
            const dealtDoorCard = doorDeck.pop()
            const dealtCard = treasureDeck.pop()
            return Promise.all([
              userGameState.addCardToHand(dealtDoorCard, newUserState.userID_FK, newUserState.gameID_FK),
              userGameState.addCardToHand(dealtCard, newUserState.userID_FK, newUserState.gameID_FK),
              gamesTable.dealCardFromTreasureDeck(dealtCard, gameObj.roomNumber),
              gamesTable.dealCardFromDoorDeck(dealtDoorCard, gameObj.roomNumber),
            ])
          }))
        })
      }))
    })
  }
}
