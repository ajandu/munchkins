const userGameState = require('../db/user_game_state');
const gamesTable = require('../db/games');

module.exports = {
  //move this to ngine functions, be sure you call updateStrength afterwards. 
  equipCards: (socket, gameObj) => {
    //you can call the equipcard function and then update strength
    console.log("entering equipCards:")
  },
  //this one is also going into ngine functions, be sure to call updatestrength afterwards 
  playLevelUpCard: (socket, gameObj) => {
  },
  //this one you might as well put into ngine functions, be sure to call updateStrength afterwards
  //basically if the gold amount from the cards you sold = 1500, then you can call the playLevelUpCard
  //function that levels you up and updates your strength. 
  sellCards: (socket, gameObj) => {

  },

  //again, this should be moved into ngine functions 
  //just be sure to update the hand and whatnot
  discardCards: (socket, gameObj) => {

  },

  //playClassCard, this might not be needed, but it will be in the engine functions as well
  //it'll just put a card into the class card space, and update the class value, then you need to call
  //update strength
  playClassCard: (socket, gameObj) => {
    
  }
}; 

