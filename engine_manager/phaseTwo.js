const userGameState = require('../db/user_game_state');
const gamesTable = require('../db/games');

module.exports = {
  //be sure after this is called after the monster fight or after the curse is applied. 
  //at this part of the turn, you'll just be able to equip cards basically
  startPhase: function (socket, gameObj) {
    return Promise.resolve().then(() => {
      return Promise.all([
        gamesTable.readDoorDeck(gameObj.roomNumber),
        gamesTable.readCardsInPlay(gameObj.roomNumber)
      ])
    }).then(([{ doorDeck }, {cardsInPlay}]) => {
      return Promise.all(Array.from({ length: 1 }, () => {
        const dealDoorCard = doorDeck.pop()
        return Promise.all([
          userGameState.addCardToHand(dealDoorCard, gameObj.userID, gameObj.roomNumber),
          gamesTable.dealCardFromDoorDeck(dealDoorCard, gameObj.roomNumber),
          gamesTable.removeCardInPlay(cardsInPlay.cardsInPlay[0], gameObj.roomNumber)
        ])
      }))
    })
  },
}