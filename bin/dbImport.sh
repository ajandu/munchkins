#!/bin/bash
node_modules/.bin/sequelize db:drop
node_modules/.bin/sequelize db:create
node_modules/.bin/sequelize db:migrate

if [ $(whoami) == "gerren" ] ; then
    psql -U postgres munchkin < node_modules/connect-pg-simple/table.sql
elif [ $(whoami) == "aculanay" ] ; then
    psql munchkin_db < node_modules/connect-pg-simple/table.sql
else    
psql munchkin < node_modules/connect-pg-simple/table.sql
fi
