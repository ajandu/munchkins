-- MySQL dump 10.13  Distrib 5.7.20, for osx10.12 (x86_64)
--
-- Host: localhost    Database: fa17g04
-- ------------------------------------------------------
-- Server version	5.7.20

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `mediaPath`
--

DROP TABLE IF EXISTS `mediaPath`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mediaPath` (
  `fk_mediaPath_addressID` int(11) NOT NULL,
  `filePath` varchar(1024) DEFAULT NULL,
  UNIQUE KEY `filePath_UNIQUE` (`filePath`),
  KEY `addressID_idx` (`fk_mediaPath_addressID`),
  CONSTRAINT `fk_mediaPath_addressID` FOREIGN KEY (`fk_mediaPath_addressID`) REFERENCES `realEstate` (`addressID`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mediaPath`
--

LOCK TABLES `mediaPath` WRITE;
/*!40000 ALTER TABLE `mediaPath` DISABLE KEYS */;
INSERT INTO `mediaPath` VALUES (2,'realEstate/2/media1FullSized.jpg'),(2,'realEstate/2/media2FullSized.jpg'),(3,'realEstate/3/IMG_2929.jpg'),(4,'realEstate/4/wassup.png'),(6,'realEstate/6/1323207125001.png'),(6,'realEstate/6/1502775729340.gif');
/*!40000 ALTER TABLE `mediaPath` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `realEstate`
--

DROP TABLE IF EXISTS `realEstate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `realEstate` (
  `addressID` int(11) NOT NULL AUTO_INCREMENT,
  `price` int(11) DEFAULT NULL,
  `description` varchar(500) DEFAULT NULL,
  `userID` int(11) DEFAULT NULL,
  `street` varchar(100) DEFAULT NULL,
  `aptNumber` varchar(15) DEFAULT NULL,
  `city` varchar(45) NOT NULL,
  `state` varchar(2) NOT NULL,
  `zipCode` varchar(5) NOT NULL,
  `bathrooms` int(11) DEFAULT NULL,
  `bedrooms` int(11) DEFAULT NULL,
  PRIMARY KEY (`addressID`),
  UNIQUE KEY `addressID_UNIQUE` (`addressID`),
  KEY `userID_idx` (`userID`),
  CONSTRAINT `userID` FOREIGN KEY (`userID`) REFERENCES `users` (`userID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `realEstate`
--

LOCK TABLES `realEstate` WRITE;
/*!40000 ALTER TABLE `realEstate` DISABLE KEYS */;
INSERT INTO `realEstate` VALUES (2,100000,'its a cardboard house',1,'1234 Fake Street','','Newark','CA','94560',2,4),(3,575000,'some house this is!',1,'6185 Jarvis Ave',' ','Newark','CA','94560',3,4),(4,492399,'Cozy place, great for kids',1,'5383 Eichler st',NULL,'Hayward','Ca','94545',2,5),(5,499999,'Doh!',1,'742 Evergreen Terrace',NULL,'Springfield','Il','45637',3,4),(6,348385,'its warm!',1,'563 Merkle Way',NULL,'Oakland','Ca','94563',2,2);
/*!40000 ALTER TABLE `realEstate` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sessions`
--

DROP TABLE IF EXISTS `sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sessions` (
  `session_id` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `expires` int(11) unsigned NOT NULL,
  `data` text CHARACTER SET utf8mb4 COLLATE utf8mb4_bin,
  PRIMARY KEY (`session_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sessions`
--

LOCK TABLES `sessions` WRITE;
/*!40000 ALTER TABLE `sessions` DISABLE KEYS */;
INSERT INTO `sessions` VALUES ('X8ghSJLNj3Vhu6OyDpIkVhxEgQ84YtIq',1513647974,'{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"httpOnly\":true,\"path\":\"/\"},\"flash\":{},\"passport\":{\"user\":{\"userName\":\"ajandu\",\"userID\":1}}}');
/*!40000 ALTER TABLE `sessions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `userID` int(11) NOT NULL AUTO_INCREMENT,
  `accountType` varchar(6) NOT NULL,
  `userName` varchar(45) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `name` varchar(120) DEFAULT NULL,
  PRIMARY KEY (`userID`),
  UNIQUE KEY `userID_UNIQUE` (`userID`),
  UNIQUE KEY `userName_UNIQUE` (`userName`),
  UNIQUE KEY `email_UNIQUE` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'admin','ajandu','amarjandu@gmail.com','$2a$10$kwNRIg2yq2yi.JV3TePwSOBFCpp18/xiHwNVLkcnL7h6pbZbpSdYK',NULL),(6,'buyer','dawg','frog@gmail.com','$2a$10$5.Ad6pAInTK4cZaMWdyfxuQP/1bLsU8Jogc9Ab6KkvdTwcNlOEFae',' wassup'),(9,'buyer','hello','helloISTHEPASS@me.com','$2a$10$kwNRIg2yq2yi.JV3TePwSOBFCpp18/xiHwNVLkcnL7h6pbZbpSdYK','hello'),(10,'buyer','test','test@test.com','$2a$10$VjZ7zxREIRH7CZ9j7viUlus4PomQixxz6xYsFxtfVCtUJ2JhLSnpG','test1'),(11,'agent','testicle','jk@gmail.com','$2a$10$q/uZYTyRchebvpY.NiYL.ulcn.S8SVG1FDgJ0500pw0Rz904y5W92','test2'),(12,'agent','dooooo','apple@gmail.com','$2a$10$PYL0R341jBQiW9DbbBuzKOTcA.VMFl4fg9wnEFDITp2Wc9gx8mU.u','hung');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-12-17 18:16:27
